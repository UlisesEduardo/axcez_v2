<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Producto
 * @package App\Models
 * @version April 4, 2020, 7:45 pm UTC
 *
 * @property string codigo
 * @property string descipcion
 * @property integer cantidad
 * @property number precioVenta
 */
class Producto extends Model
{
    use SoftDeletes;

    public $table = 'productos';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'codigo',
        'descipcion',
        'cantidad',
        'precioVenta'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'codigo' => 'string',
        'descipcion' => 'string',
        'cantidad' => 'integer',
        'precioVenta' => 'double'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'codigo' => 'required|max:10|min:3',
        'descipcion' => 'required|regex:/^[\pL\s\-]+$/u',
        'cantidad' => 'required|numeric',
        'precioVenta' => 'required|numeric'
    ];

    
}

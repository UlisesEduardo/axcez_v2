<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Pendiente
 * @package App\Models
 * @version April 5, 2020, 5:26 pm UTC
 *
 * @property string codigoCliente
 * @property string motivo
 * @property string fechaProgramada
 * @property string estatus
 * @property string urgencia
 * @property string velocidadRed
 * @property string notas
 * @property string usuario
 */
class Pendiente extends Model
{
    use SoftDeletes;

    public $table = 'pendientes';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'codigoCliente',
        'motivo',
        'fechaProgramada',
        'estatus',
        'urgencia',
        'velocidadRed',
        'notas',
        'usuario'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'codigoCliente' => 'string',
        'motivo' => 'string',
        'fechaProgramada' => 'date',
        'estatus' => 'string',
        'urgencia' => 'string',
        'velocidadRed' => 'string',
        'notas' => 'string',
        'usuario' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'codigoCliente' => 'required|min:7|max:7|regex:(^([AXZ]+)(\d+)?$)',
        'motivo' => 'required|regex:/^[\pL\s\-]+$/u|max:50',
        'fechaProgramada' => 'required',
        'estatus' => 'required',
        'urgencia' => 'required',
        'velocidadRed' => 'required|max:21',
        'notas' => 'max:150'
    ];

    
}

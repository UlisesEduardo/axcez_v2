<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Contrato
 * @package App\Models
 * @version April 10, 2020, 4:33 pm UTC
 *
 * @property string folio
 * @property string fechaContrato
 * @property string notasAdicionales
 * @property string fechaInstalacion
 * @property integer id_sector
 * @property integer id_plan
 * @property string ip
 * @property string gateway
 * @property string fechaPago
 * @property number montoReal
 * @property number montoCobrado
 * @property string tipoContrato
 */
class Contrato extends Model
{
    use SoftDeletes;

    public $table = 'contratos';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'folio',
        'fechaContrato',
        'notasAdicionales',
        'fechaInstalacion',
        'id_sector',
        'id_plan',
        'ip',
        'gateway',
        'fechaPago',
        'montoReal',
        'montoCobrado',
        'tipoContrato'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'folio' => 'string',
        'fechaContrato' => 'date',
        'notasAdicionales' => 'string',
        'fechaInstalacion' => 'date',
        'id_sector' => 'integer',
        'id_plan' => 'integer',
        'ip' => 'string',
        'gateway' => 'string',
        'fechaPago' => 'string',
        'montoReal' => 'double',
        'montoCobrado' => 'double',
        'tipoContrato' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'fechaContrato' => 'required',
        'fechaInstalacion' => 'required',
        'id_sector' => 'required',
        'id_plan' => 'required',
        'ip' => 'required|ip|max:15',
        'gateway' => 'required|ip|max:15',
        'fechaPago' => 'required',
        'montoReal' => 'required|numeric',
        'montoCobrado' => 'required|numeric',
        'tipoContrato' => 'required'
    ];

    public function sector(){
		return $this->hasOne(Sectorial::class,'id','id_sector');
    }
    
    public function plan(){
        return $this->hasOne(Plan::class,'id','id_plan');
    }
}

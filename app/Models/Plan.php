<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Plan
 * @package App\Models
 * @version April 4, 2020, 6:13 pm UTC
 *
 * @property integer id_region
 * @property string nombre
 * @property string subida
 * @property string bajada
 * @property number precio
 * @property number precioMin
 * @property number precioMax
 * @property string descripcion
 */
class Plan extends Model
{
    use SoftDeletes;

    public $table = 'plans';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'id_region',
        'nombre',
        'subida',
        'bajada',
        'precio',
        'precioMin',
        'precioMax',
        'descripcion'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'id_region' => 'integer',
        'nombre' => 'string',
        'subida' => 'string',
        'bajada' => 'string',
        'precio' => 'double',
        'precioMin' => 'double',
        'precioMax' => 'double',
        'descripcion' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'id_region' => 'required',
        'nombre' => 'required|string',
        'subida' => 'required|min:4|max:6',
        'bajada' => 'required|min:4|max:6',
        'precio' => 'required|numeric',
        'precioMin' => 'required|numeric',
        'precioMax' => 'required|numeric',
        'descripcion' => 'required|regex:/^[\pL\s\-]+$/u|max:150'
    ];

    public function region(){
        return $this->hasOne('App\Models\Region', 'id','id_region');
    }

}

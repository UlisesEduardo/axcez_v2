<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Compras
 * @package App\Models
 * @version April 8, 2020, 3:16 am UTC
 *
 * @property string fecha
 * @property number total
 * @property string proveedor
 * @property string usuario
 */
class Compras extends Model
{
    use SoftDeletes;

    public $table = 'compras';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'fecha',
        'total',
        'proveedor',
        'usuario'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'fecha' => 'date',
        'total' => 'double',
        'proveedor' => 'string',
        'usuario' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'fecha' => 'required',
        'total' => 'required',
        'proveedor' => 'required',
    ];

}

<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Proveedor
 * @package App\Models
 * @version April 14, 2020, 7:57 pm UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection compras
 * @property string nombre
 * @property string telefonoContacto
 * @property string telefonoSecundario
 * @property string correo
 * @property string encargado
 * @property string ubicacion
 * @property string pagina
 */
class Proveedor extends Model
{
    use SoftDeletes;

    public $table = 'proveedors';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'nombre',
        'telefonoContacto',
        'telefonoSecundario',
        'correo',
        'encargado',
        'ubicacion',
        'pagina'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'nombre' => 'string',
        'telefonoContacto' => 'string',
        'telefonoSecundario' => 'string',
        'correo' => 'string',
        'encargado' => 'string',
        'ubicacion' => 'string',
        'pagina' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'nombre' => 'required',
        'telefonoContacto' => 'required|regex:/^[0-9]+$/|min:10|max:10',
        'telefonoSecundario' => 'required|regex:/^[0-9]+$/|min:10|max:10',
        'correo' => 'required|email|max:50',
        'encargado' => 'required|regex:/^[\pL\s\-]+$/u|max:50',
        'ubicacion' => 'required|max:100',
        'pagina' => 'required|url|max:100'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    
}

<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Gastos
 * @package App\Models
 * @version April 4, 2020, 8:00 pm UTC
 *
 * @property string concepto
 * @property number total
 * @property string fecha
 * @property string usuario
 */
class Gastos extends Model
{
    use SoftDeletes;

    public $table = 'gastos';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'concepto',
        'total',
        'fecha',
        'usuario'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'concepto' => 'string',
        'total' => 'double',
        'fecha' => 'date',
        'usuario' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'concepto' => 'required|max:100',
        'total' => 'required|numeric',
        'fecha' => 'required',
    ];

    
}

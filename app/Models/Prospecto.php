<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Prospecto
 * @package App\Models
 * @version April 4, 2020, 7:25 pm UTC
 *
 * @property string nombres
 * @property string apellidoPaterno
 * @property string apellidoMaterno
 * @property string municipio
 * @property string barrio
 * @property string calle
 * @property string num
 * @property string referencia
 * @property string coordenadas
 * @property string telefono
 * @property string telefonoAlternativo
 * @property string correo
 * @property string notas
 */
class Prospecto extends Model
{
    use SoftDeletes;

    public $table = 'prospectos';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'nombres',
        'apellidoPaterno',
        'apellidoMaterno',
        'municipio',
        'barrio',
        'calle',
        'num',
        'referencia',
        'coordenadas',
        'telefono',
        'telefonoAlternativo',
        'correo',
        'notas'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'nombres' => 'string',
        'apellidoPaterno' => 'string',
        'apellidoMaterno' => 'string',
        'municipio' => 'string',
        'barrio' => 'string',
        'calle' => 'string',
        'num' => 'string',
        'referencia' => 'string',
        'coordenadas' => 'string',
        'telefono' => 'string',
        'telefonoAlternativo' => 'string',
        'correo' => 'string',
        'notas' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'nombres' => 'required|regex:/^[\pL\s\-]+$/u|max:50',
        'apellidoPaterno' => 'required|regex:/^[\pL\s\-]+$/u|max:50',
        'apellidoMaterno' => 'required|regex:/^[\pL\s\-]+$/u|max:50',
        'municipio' => 'required|regex:/^[\pL\s\-]+$/u|max:50',
        'barrio' => 'required|regex:/^[\pL\s\-]+$/u|max:50',
        'num' => 'required|regex:/^[0-9]+$/|min:1|max:5',
        'referencia' => 'required|max:100',
        'coordenadas' => 'required|max:100',
        'calle' => 'required|max:50',
        'telefono' => 'required|regex:/^[0-9]+$/|min:10|max:10',
        'telefonoAlternativo' => 'required|regex:/^[0-9]+$/|min:10|max:10',
        'correo' => 'required|email|max:50'
    ];

    
}

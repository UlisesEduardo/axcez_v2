<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Pago
 * @package App\Models
 * @version April 12, 2020, 7:13 pm UTC
 *
 * @property number importe
 * @property string proximoPago
 * @property string fechaInicio
 * @property string fechaFin
 * @property string notas
 */
class Pago extends Model
{
    use SoftDeletes;

    public $table = 'pagos';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'importe',
        'proximoPago',
        'fechaInicio',
        'fechaFin',
        'notas'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'importe' => 'double',
        'proximoPago' => 'date',
        'fechaInicio' => 'date',
        'fechaFin' => 'date',
        'notas' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}

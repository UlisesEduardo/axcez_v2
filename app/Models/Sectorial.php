<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Sectorial
 * @package App\Models
 * @version April 4, 2020, 6:23 pm UTC
 *
 * @property integer id_region
 * @property string nombre
 * @property string ip
 * @property string gateway
 * @property string notas
 * @property string frecuencia
 */
class Sectorial extends Model
{
    use SoftDeletes;

    public $table = 'sectorials';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'id_region',
        'nombre',
        'ip',
        'gateway',
        'notas',
        'frecuencia'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'id_region' => 'integer',
        'nombre' => 'string',
        'ip' => 'string',
        'gateway' => 'string',
        'notas' => 'string',
        'frecuencia' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'id_region' => 'required',
        'nombre' => 'required|max:50',
        'ip' => 'required|ip|max:15',
        'gateway' => 'required|ip|max:15',
        'notas' => 'required|max:150',
        'frecuencia' => 'required|max:50'
    ];

    public function regionSectorial(){
        return $this->hasOne('App\Models\Region', 'id','id_region');
    }
    
}

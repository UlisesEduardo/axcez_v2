<?php

namespace App\Repositories;

use App\Models\Prospecto;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class ProspectoRepository
 * @package App\Repositories
 * @version April 4, 2020, 7:25 pm UTC
 *
 * @method Prospecto findWithoutFail($id, $columns = ['*'])
 * @method Prospecto find($id, $columns = ['*'])
 * @method Prospecto first($columns = ['*'])
*/
class ProspectoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nombres',
        'apellidoPaterno',
        'apellidoMaterno',
        'municipio',
        'barrio',
        'calle',
        'num',
        'referencia',
        'coordenadas',
        'telefono',
        'telefonoAlternativo',
        'correo',
        'notas'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Prospecto::class;
    }
}

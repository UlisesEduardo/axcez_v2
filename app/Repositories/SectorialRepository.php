<?php

namespace App\Repositories;

use App\Models\Sectorial;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class SectorialRepository
 * @package App\Repositories
 * @version April 4, 2020, 6:23 pm UTC
 *
 * @method Sectorial findWithoutFail($id, $columns = ['*'])
 * @method Sectorial find($id, $columns = ['*'])
 * @method Sectorial first($columns = ['*'])
*/
class SectorialRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id_region',
        'nombre',
        'ip',
        'gateway',
        'notas',
        'frecuencia'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Sectorial::class;
    }
}

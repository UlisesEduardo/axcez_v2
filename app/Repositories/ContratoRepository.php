<?php

namespace App\Repositories;

use App\Models\Contrato;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class ContratoRepository
 * @package App\Repositories
 * @version April 10, 2020, 4:33 pm UTC
 *
 * @method Contrato findWithoutFail($id, $columns = ['*'])
 * @method Contrato find($id, $columns = ['*'])
 * @method Contrato first($columns = ['*'])
*/
class ContratoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'folio',
        'fechaContrato',
        'notasAdicionales',
        'fechaInstalacion',
        'id_sector',
        'id_plan',
        'ip',
        'gateway',
        'fechaPago',
        'montoReal',
        'montoCobrado',
        'tipoContrato'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Contrato::class;
    }
}

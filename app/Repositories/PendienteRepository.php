<?php

namespace App\Repositories;

use App\Models\Pendiente;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class PendienteRepository
 * @package App\Repositories
 * @version April 5, 2020, 5:26 pm UTC
 *
 * @method Pendiente findWithoutFail($id, $columns = ['*'])
 * @method Pendiente find($id, $columns = ['*'])
 * @method Pendiente first($columns = ['*'])
*/
class PendienteRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'codigoCliente',
        'motivo',
        'fechaProgramada',
        'estatus',
        'urgencia',
        'velocidadRed',
        'notas',
        'usuario'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Pendiente::class;
    }
}

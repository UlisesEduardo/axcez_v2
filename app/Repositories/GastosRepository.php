<?php

namespace App\Repositories;

use App\Models\Gastos;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class GastosRepository
 * @package App\Repositories
 * @version April 4, 2020, 8:00 pm UTC
 *
 * @method Gastos findWithoutFail($id, $columns = ['*'])
 * @method Gastos find($id, $columns = ['*'])
 * @method Gastos first($columns = ['*'])
*/
class GastosRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'concepto',
        'total',
        'fecha',
        'usuario'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Gastos::class;
    }
}

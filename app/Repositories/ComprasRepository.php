<?php

namespace App\Repositories;

use App\Models\Compras;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class ComprasRepository
 * @package App\Repositories
 * @version April 8, 2020, 3:16 am UTC
 *
 * @method Compras findWithoutFail($id, $columns = ['*'])
 * @method Compras find($id, $columns = ['*'])
 * @method Compras first($columns = ['*'])
*/
class ComprasRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'fecha',
        'total',
        'proveedor',
        'usuario'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Compras::class;
    }
}

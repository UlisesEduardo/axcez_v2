<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatePagoRequest;
use App\Http\Requests\UpdatePagoRequest;
use App\Repositories\PagoRepository;
use App\Http\Controllers\AppBaseController;
use App\Models\Pago;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Illuminate\Support\Facades\DB;

class PagoController extends AppBaseController
{
    /** @var  PagoRepository */
    private $pagoRepository;
    var $STORE;
    var $ERROR;

    public function __construct(PagoRepository $pagoRepo)
    {
        $this->middleware('permission:pagos.index')->only('index');
        $this->middleware('permission:pagos.create')->only(['create','store']);
        $this->middleware('permission:pagos.show')->only('show');
        $this->pagoRepository = $pagoRepo;
        $this->STORE = 'Datos registrados de forma exitosa';
        $this->ERROR = 'Datos no encontrados';
    }

    /**
     * Display a listing of the Pago.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->pagoRepository->pushCriteria(new RequestCriteria($request));
        //$pagos = $this->pagoRepository->all();
        $pagos = Pago::paginate(10);
        return view('pagos.index')
            ->with('pagos', $pagos);
    }

    /**
     * Show the form for creating a new Pago.
     *
     * @return Response
     */
    public function create()
    {
        return view('pagos.create');
    }

    /**
     * Store a newly created Pago in storage.
     *
     * @param CreatePagoRequest $request
     *
     * @return Response
     */
    /*public function store(CreatePagoRequest $request)
    {
        $input = $request->all();

        $pago = $this->pagoRepository->create($input);

        Flash::success('Pago saved successfully.');

        return redirect(route('pagos.index'));
    }*/

    public function store(Request $request)
    {
        if ($request->ajax()) {
            $folioContrato = $request->input('folioContrato');
            $importe = $request->input('importe');
            $proximoPago = $request->input('proximoPago');
            $fechaInicio = $request->input('fechaInicio');
            $fechaFin = $request->input('fechaFin');
            $condonado = $request->input('condonado');
            $notas = $request->input('notas');

            $id_c = DB::select('select id from contratos where folio = ?', [$folioContrato]);
           
            if (empty($id_c)) {
                Flash::error($this->ERROR);

                return redirect(route('pagos.index'));
            }
            $id_contrato = $id_c[0]->id;
            DB::insert(
                'insert into pagos(id_contrato
            ,importe
            ,proximoPago
            ,fechaInicio
            ,fechaFin
            ,condonado
            ,notas
            ,created_at) values(?,?,?,?,?,?,?,?)',
                [
                    $id_contrato, $importe, $proximoPago, $fechaInicio, $fechaFin, $condonado, $notas, now()->format('Y-m-d')
                ]
            );
        }

        Flash::success($this->STORE);

        return redirect(route('pagos.index'));
    }

    /**
     * Display the specified Pago.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $pago = $this->pagoRepository->findWithoutFail($id);

        if (empty($pago)) {
            Flash::error($this->ERROR);

            return redirect(route('pagos.index'));
        }

        return view('pagos.show')->with('pago', $pago);
    }
}

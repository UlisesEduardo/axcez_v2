<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateProspectoRequest;
use App\Http\Requests\UpdateProspectoRequest;
use App\Repositories\ProspectoRepository;
use App\Http\Controllers\AppBaseController;
use App\Models\Prospecto;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class ProspectoController extends AppBaseController
{
    /** @var  ProspectoRepository */
    private $prospectoRepository;
    var $STORE;
    var $ERROR;
    var $UPDATE;
    var $DELETE;


    public function __construct(ProspectoRepository $prospectoRepo)
    {
        $this->middleware('permission:prospectos.index')->only('index');
        $this->middleware('permission:prospectos.create')->only(['create','store']);
        $this->middleware('permission:prospectos.show')->only('show');
        $this->middleware('permission:prospectos.edit')->only(['edit','update']);
        $this->middleware('permission:prospectos.destroy')->only('destroy');
        $this->prospectoRepository = $prospectoRepo;
        $this->STORE = 'Datos registrados de forma exitosa';
        $this->ERROR = 'Datos no encontrados';
        $this->UPDATE = 'Datos actualizados de forma exitosa';
        $this->DELETE = 'Registro correctamente eliminado';
    }

    /**
     * Display a listing of the Prospecto.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->prospectoRepository->pushCriteria(new RequestCriteria($request));
        //$prospectos = $this->prospectoRepository->all();
        $prospectos = Prospecto::paginate(10);
        return view('prospectos.index')
            ->with('prospectos', $prospectos);
    }

    /**
     * Show the form for creating a new Prospecto.
     *
     * @return Response
     */
    public function create()
    {
        return view('prospectos.create');
    }

    /**
     * Store a newly created Prospecto in storage.
     *
     * @param CreateProspectoRequest $request
     *
     * @return Response
     */
    public function store(CreateProspectoRequest $request)
    {
        $input = $request->all();

        $prospecto = $this->prospectoRepository->create($input);

        Flash::success($this->STORE);

        return redirect(route('prospectos.index'));
    }

    /**
     * Display the specified Prospecto.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $prospecto = $this->prospectoRepository->findWithoutFail($id);

        if (empty($prospecto)) {
            Flash::error($this->ERROR);

            return redirect(route('prospectos.index'));
        }

        return view('prospectos.show')->with('prospecto', $prospecto);
    }

    /**
     * Show the form for editing the specified Prospecto.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $prospecto = $this->prospectoRepository->findWithoutFail($id);

        if (empty($prospecto)) {
            Flash::error($this->ERROR);

            return redirect(route('prospectos.index'));
        }

        return view('prospectos.edit')->with('prospecto', $prospecto);
    }

    /**
     * Update the specified Prospecto in storage.
     *
     * @param  int              $id
     * @param UpdateProspectoRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateProspectoRequest $request)
    {
        $prospecto = $this->prospectoRepository->findWithoutFail($id);

        if (empty($prospecto)) {
            Flash::error($this->ERROR);

            return redirect(route('prospectos.index'));
        }

        $prospecto = $this->prospectoRepository->update($request->all(), $id);

        Flash::success($this->UPDATE);

        return redirect(route('prospectos.index'));
    }

    /**
     * Remove the specified Prospecto from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $prospecto = $this->prospectoRepository->findWithoutFail($id);

        if (empty($prospecto)) {
            Flash::error($this->ERROR);

            return redirect(route('prospectos.index'));
        }

        $this->prospectoRepository->delete($id);

        Flash::success($this->DELETE);

        return redirect(route('prospectos.index'));
    }
}

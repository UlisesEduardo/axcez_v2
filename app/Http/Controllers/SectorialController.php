<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateSectorialRequest;
use App\Http\Requests\UpdateSectorialRequest;
use App\Repositories\SectorialRepository;
use App\Http\Controllers\AppBaseController;
use App\Models\Sectorial;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Illuminate\Support\Facades\DB;

class SectorialController extends AppBaseController
{
    /** @var  SectorialRepository */
    private $sectorialRepository;
    var $STORE;
    var $ERROR;
    var $UPDATE;
    var $DELETE;

    public function __construct(SectorialRepository $sectorialRepo)
    {
        $this->middleware('permission:sectorials.index')->only('index');
        $this->middleware('permission:sectorials.create')->only(['create','store']);
        $this->middleware('permission:sectorials.show')->only('show');
        $this->middleware('permission:sectorials.edit')->only(['edit','update']);
        $this->middleware('permission:sectorials.destroy')->only('destroy');
        $this->sectorialRepository = $sectorialRepo;
        $this->STORE = 'Datos registrados de forma exitosa';
        $this->ERROR = 'Datos no encontrados';
        $this->UPDATE = 'Datos actualizados de forma exitosa';
        $this->DELETE = 'Registro correctamente eliminado';
    }

    /**
     * Display a listing of the Sectorial.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->sectorialRepository->pushCriteria(new RequestCriteria($request));
        //$sectorials = $this->sectorialRepository->all();
        $sectorials = Sectorial::paginate(10);
        return view('sectorials.index')
            ->with('sectorials', $sectorials);
    }

    /**
     * Show the form for creating a new Sectorial.
     *
     * @return Response
     */
    public function create()
    {
        return view('sectorials.create');
    }

    /**
     * Store a newly created Sectorial in storage.
     *
     * @param CreateSectorialRequest $request
     *
     * @return Response
     */
    public function store(CreateSectorialRequest $request)
    {
        $input = $request->all();

        $sectorial = $this->sectorialRepository->create($input);

        Flash::success($this->STORE);

        return redirect(route('sectorials.index'));
    }

    /**
     * Display the specified Sectorial.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $sectorial = $this->sectorialRepository->findWithoutFail($id);

        if (empty($sectorial)) {
            Flash::error($this->ERROR);

            return redirect(route('sectorials.index'));
        }

        $contratos = DB::select('call sp_sectorialContrato(?)',[$id]);
        
        return view('sectorials.show',compact('sectorial','contratos'));
    }

    /**
     * Show the form for editing the specified Sectorial.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $sectorial = $this->sectorialRepository->findWithoutFail($id);

        if (empty($sectorial)) {
            Flash::error($this->ERROR);

            return redirect(route('sectorials.index'));
        }

        return view('sectorials.edit')->with('sectorial', $sectorial);
    }

    /**
     * Update the specified Sectorial in storage.
     *
     * @param  int              $id
     * @param UpdateSectorialRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSectorialRequest $request)
    {
        $sectorial = $this->sectorialRepository->findWithoutFail($id);

        if (empty($sectorial)) {
            Flash::error($this->ERROR);

            return redirect(route('sectorials.index'));
        }

        $sectorial = $this->sectorialRepository->update($request->all(), $id);

        Flash::success($this->UPDATE);

        return redirect(route('sectorials.index'));
    }

    /**
     * Remove the specified Sectorial from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $sectorial = $this->sectorialRepository->findWithoutFail($id);

        if (empty($sectorial)) {
            Flash::error($this->ERROR);

            return redirect(route('sectorials.index'));
        }

        $this->sectorialRepository->delete($id);

        Flash::success($this->DELETE);

        return redirect(route('sectorials.index'));
    }
}

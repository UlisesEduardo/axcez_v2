<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateGastosRequest;
use App\Http\Requests\UpdateGastosRequest;
use App\Repositories\GastosRepository;
use App\Http\Controllers\AppBaseController;
use App\Models\Gastos;
use Illuminate\Http\Request;
use Flash;
use Illuminate\Support\Facades\Auth;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class GastosController extends AppBaseController
{
    /** @var  GastosRepository */
    private $gastosRepository;
    var $STORE;
    var $ERROR;
    var $UPDATE;
    var $DELETE;

    public function __construct(GastosRepository $gastosRepo)
    {
        $this->middleware('permission:gastos.index')->only('index');
        $this->middleware('permission:gastos.create')->only(['create','store']);
        $this->middleware('permission:gastos.show')->only('show');
        $this->middleware('permission:gastos.edit')->only(['edit','update']);
        $this->middleware('permission:gastos.destroy')->only('destroy');
        $this->gastosRepository = $gastosRepo;
        $this->STORE = 'Datos registrados de forma exitosa';
        $this->ERROR = 'Datos no encontrados';
        $this->UPDATE = 'Datos actualizados de forma exitosa';
        $this->DELETE = 'Registro correctamente eliminado';
    }

    /**
     * Display a listing of the Gastos.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->gastosRepository->pushCriteria(new RequestCriteria($request));
        //$gastos = $this->gastosRepository->all();
        $gastos = Gastos::paginate(10);
        return view('gastos.index')
            ->with('gastos', $gastos);
    }

    /**
     * Show the form for creating a new Gastos.
     *
     * @return Response
     */
    public function create()
    {
        return view('gastos.create');
    }

    /**
     * Store a newly created Gastos in storage.
     *
     * @param CreateGastosRequest $request
     *
     * @return Response
     */
    public function store(CreateGastosRequest $request)
    {
        $input = $request->all();
        //al input le agregamos el usuario que hace el registro a través de la sesión
        $input['usuario'] = Auth::user()->name;
        $gastos = $this->gastosRepository->create($input);

        Flash::success($this->STORE);

        return redirect(route('gastos.index'));
    }

    /**
     * Display the specified Gastos.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $gastos = $this->gastosRepository->findWithoutFail($id);

        if (empty($gastos)) {
            Flash::error($this->ERROR);

            return redirect(route('gastos.index'));
        }

        return view('gastos.show')->with('gastos', $gastos);
    }

    /**
     * Show the form for editing the specified Gastos.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $gastos = $this->gastosRepository->findWithoutFail($id);

        if (empty($gastos)) {
            Flash::error($this->ERROR);

            return redirect(route('gastos.index'));
        }

        return view('gastos.edit')->with('gastos', $gastos);
    }

    /**
     * Update the specified Gastos in storage.
     *
     * @param  int              $id
     * @param UpdateGastosRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateGastosRequest $request)
    {
        $gastos = $this->gastosRepository->findWithoutFail($id);

        if (empty($gastos)) {
            Flash::error($this->ERROR);

            return redirect(route('gastos.index'));
        }

        $gastos = $this->gastosRepository->update($request->all(), $id);

        Flash::success($this->UPDATE);

        return redirect(route('gastos.index'));
    }

    /**
     * Remove the specified Gastos from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $gastos = $this->gastosRepository->findWithoutFail($id);

        if (empty($gastos)) {
            Flash::error($this->ERROR);

            return redirect(route('gastos.index'));
        }

        $this->gastosRepository->delete($id);

        Flash::success($this->DELETE);

        return redirect(route('gastos.index'));
    }
}

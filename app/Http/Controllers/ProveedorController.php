<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateProveedorRequest;
use App\Http\Requests\UpdateProveedorRequest;
use App\Repositories\ProveedorRepository;
use App\Http\Controllers\AppBaseController;
use App\Models\Proveedor;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class ProveedorController extends AppBaseController
{
    /** @var  ProveedorRepository */
    private $proveedorRepository;
    var $STORE;
    var $ERROR;
    var $UPDATE;
    var $DELETE;

    public function __construct(ProveedorRepository $proveedorRepo)
    {
        $this->middleware('permission:proveedors.index')->only('index');
        $this->middleware('permission:proveedors.create')->only(['create','store']);
        $this->middleware('permission:proveedors.show')->only('show');
        $this->middleware('permission:proveedors.edit')->only(['edit','update']);
        $this->middleware('permission:proveedors.destroy')->only('destroy');
        $this->proveedorRepository = $proveedorRepo;
        $this->STORE = 'Datos registrados de forma exitosa';
        $this->ERROR = 'Datos no encontrados';
        $this->UPDATE = 'Datos actualizados de forma exitosa';
        $this->DELETE = 'Registro correctamente eliminado';
    }

    /**
     * Display a listing of the Proveedor.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->proveedorRepository->pushCriteria(new RequestCriteria($request));
        //$proveedors = $this->proveedorRepository->all();
        $proveedors = Proveedor::paginate(10);
        return view('proveedors.index')
            ->with('proveedors', $proveedors);
    }

    /**
     * Show the form for creating a new Proveedor.
     *
     * @return Response
     */
    public function create()
    {
        return view('proveedors.create');
    }

    /**
     * Store a newly created Proveedor in storage.
     *
     * @param CreateProveedorRequest $request
     *
     * @return Response
     */
    public function store(CreateProveedorRequest $request)
    {
        $input = $request->all();

        $proveedor = $this->proveedorRepository->create($input);

        Flash::success($this->STORE);

        return redirect(route('proveedors.index'));
    }

    /**
     * Display the specified Proveedor.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $proveedor = $this->proveedorRepository->findWithoutFail($id);

        if (empty($proveedor)) {
            Flash::error($this->ERROR);

            return redirect(route('proveedors.index'));
        }

        return view('proveedors.show')->with('proveedor', $proveedor);
    }

    /**
     * Show the form for editing the specified Proveedor.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $proveedor = $this->proveedorRepository->findWithoutFail($id);

        if (empty($proveedor)) {
            Flash::error($this->ERROR);

            return redirect(route('proveedors.index'));
        }

        return view('proveedors.edit')->with('proveedor', $proveedor);
    }

    /**
     * Update the specified Proveedor in storage.
     *
     * @param  int              $id
     * @param UpdateProveedorRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateProveedorRequest $request)
    {
        $proveedor = $this->proveedorRepository->findWithoutFail($id);

        if (empty($proveedor)) {
            Flash::error($this->ERROR);

            return redirect(route('proveedors.index'));
        }

        $proveedor = $this->proveedorRepository->update($request->all(), $id);

        Flash::success($this->UPDATE);

        return redirect(route('proveedors.index'));
    }

    /**
     * Remove the specified Proveedor from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $proveedor = $this->proveedorRepository->findWithoutFail($id);

        if (empty($proveedor)) {
            Flash::error($this->ERROR);

            return redirect(route('proveedors.index'));
        }

        $this->proveedorRepository->delete($id);

        Flash::success($this->DELETE);

        return redirect(route('proveedors.index'));
    }
}

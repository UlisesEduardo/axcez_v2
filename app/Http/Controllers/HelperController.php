<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use App\Models\Cliente;
use App\Models\Producto;
use Laracasts\Flash\Flash;

class HelperController extends Controller
{
    /*
     * Este método hace una busqueda de clientes
     */
    public function busqueda(Request $request)
    {
        $nombres = $request->input('nombres');
        //verificamos si existen datos de entrada sino no se emite respuesta alguna
        if($nombres){
            $clientes = Cliente::where('nombres', 'LIKE', "%$nombres%")->get();
            //return view('pendientes.create',compact('clientes'));
            return response()->json($clientes);

        }
    }

    /*
     * Este método hace una busqueda de productos según su codigo, si esta registrado
     * se manda un success
     */
    public function buscaProducto(Request $request)
    {
        $codigo = $request->input('codigo');
        //$producto = Producto::where('codigo', "$codigo")->get();
        $producto = DB::select('select codigo from productos where codigo = ?', [$codigo]);
        if(sizeof($producto)>0){
            return response()->json($producto);
        }
    }

     /*
     * Este método verifica que existan suficientes productos para vender, de ser así
     * se manda un succes
     */
    public function verificarStock(Request $request)
    {
        $codigo = $request->input('codigoProducto');
        $cantidadSolicitada = $request->input('cantidadSolicitada');
        $cantidadProducto = DB::select('select cantidad from productos where codigo = ?', [$codigo]);
        if($cantidadProducto[0]->cantidad>=$cantidadSolicitada){
            return response()->json($cantidadProducto);
        }
    }

    /*
     * Este método verifica el contrato asignado a un cliente, recibe el nombre del cliente 
     * y mediante un store obtiene el o los contratos ligados al cliente
     */
    public function verificarContrato(Request $request)
    {
        $nombres = $request->input('nombres');
        //verificamos si existen datos de entrada
        if($nombres){
            $clientes = DB::select('call sp_verificarContrato(?)',[$nombres]);
            //return view('pendientes.create',compact('clientes'));
            return response()->json($clientes);
        }
    }

    /*
     * Este método verifica que exista el codigo del cliente, el cual es recibido del input,
     * si el codigo existe, se envian los datos del cliente
     */
    public function verificarFolio(Request $request)
    {
        $codigoCliente = $request->input('codigoCliente');
        //verificamos si existen datos de entrada
        $cliente = DB::select('select * from clientes where codigo = ?',[$codigoCliente]);
        if(sizeof($cliente)>0){
            //return view('pendientes.create',compact('clientes'));
            return response()->json($cliente);
        }
    }

    public function verificarSector(Request $request)
    {
        $id_sector = $request->input('id_sector');
        //verificamos si existen datos de entrada
        $sector = DB::select('select ip,gateway from sectorials where id = ?',[$id_sector]);
        if(sizeof($sector)>0){
            //return view('pendientes.create',compact('clientes'));
            return response()->json($sector);
        }
    }

    /*
     * Este método hace una busqueda de contratos con base al nombre de un cliente
     */
    public function _contratoCliente(Request $request)
    {
        $nombres = $request->input('datosCliente');
        //verificamos si existen datos de entrada sino no se emite respuesta alguna
        if($nombres){
            $contrato = DB::select('call sp_contratoCliente(?)',[$nombres]);
            return response()->json($contrato);
        }
    }
    
    public function contratoCliente(Request $request)
    {
        $nombres = $request->input('datosCliente');
        //verificamos si existen datos de entrada sino no se emite respuesta alguna
        $contratos = DB::select('call sp_contratoCliente(?)',[$nombres]);
        $view = \View::make('contratos.listaContratos')->with('contratos',$contratos);
        if($request->ajax()){
            $sections = $view->renderSections();
            return \Response::json($sections['contentPanel']); 
        }else return $view;
    }

    public function pagoCliente(Request $request)
    {
        $nombres = $request->input('datosCliente');
        //verificamos si existen datos de entrada sino no se emite respuesta alguna
        $pagos = DB::select('call sp_pagoCliente(?)',[$nombres]);
        $view = \View::make('pagos.listaPagos')->with('pagos',$pagos);
        if($request->ajax()){
            $sections = $view->renderSections();
            return \Response::json($sections['contentPanel']); 
        }else return $view;
    }

    public function datosCliente(Request $request)
    {
        $nombres = $request->input('datosCliente');
        //verificamos si existen datos de entrada sino no se emite respuesta alguna
        $clientes = DB::select('call sp_datosCliente(?)',[$nombres]);
        $view = \View::make('clientes.listaClientes')->with('clientes',$clientes);
        if($request->ajax()){
            $sections = $view->renderSections();
            return \Response::json($sections['contentPanel']); 
        }else return $view;
    }

    public function respaldar()
    {
        //cambiar la ruta y el contenido del archivo bat
       exec('C:\Users\cliente-preferido\Desktop\respaldo_base_de_datos.bat');
       Flash::success('Respaldo creado de forma exitosa');
       return redirect('/home');
    }

    public function generarPdf()
    {
     $gastos = DB::select('SELECT concepto, total FROM gastos WHERE fecha = CURDATE()');
     $compras = DB::select('call sp_informeCompras()');
     $condonados = DB::select('call sp_informeCondonados()');
     $pagos = DB::select('call sp_informePagos()');
     $contratos = DB::select('SELECT folio,montoCobrado FROM contratos WHERE fechaContrato = CURDATE()');
        return \PDF::loadView('reporte_diario',compact('gastos','compras','condonados','pagos','contratos'))
        ->setPaper('letter')
        ->stream('informe_diario.pdf');
    }
}

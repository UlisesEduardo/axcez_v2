<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateComprasRequest;
use App\Http\Requests\UpdateComprasRequest;
use App\Repositories\ComprasRepository;
use App\Http\Controllers\AppBaseController;
use App\Models\Compras;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Illuminate\Support\Facades\DB;
use App\Models\Proveedor;
use Illuminate\Support\Facades\Auth;

class ComprasController extends AppBaseController
{
    /** @var  ComprasRepository */
    private $comprasRepository;
    var $STORE;
    var $ERROR;

    public function __construct(ComprasRepository $comprasRepo)
    {
        $this->middleware('permission:compras.index')->only('index');
        $this->middleware('permission:compras.create')->only(['create','store']);
        $this->middleware('permission:compras.show')->only('show');
        $this->comprasRepository = $comprasRepo;
        $this->STORE = 'Datos registrados de forma exitosa';
        $this->ERROR = 'Datos no encontrados';
    }

    /**
     * Display a listing of the Compras.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        //$this->comprasRepository->pushCriteria(new RequestCriteria($request));
        //$compras = $this->comprasRepository->all();
        //$compras = Compras::paginate(10);
        $compras = DB::table('compras')
        ->join('proveedors', 'compras.id_proveedor', '=', 'proveedors.id')
        ->select('compras.id', 'compras.fecha', 'proveedors.nombre', 'compras.total', 'compras.usuario')
        ->paginate(10);
        return view('compras.index')
            ->with('compras', $compras);
    }

    /**
     * Show the form for creating a new Compras.
     *
     * @return Response
     */
    public function create()
    {
        $proveedores = Proveedor::pluck('nombre','id');
        $productos = DB::select('select codigo,descipcion from productos');
        return view('compras.create',compact('productos','proveedores'));
    }

    /**
     * Store a newly created Compras in storage.
     *
     * @param CreateComprasRequest $request
     *
     * @return Response
     */
    public function store(Request $request)
    {
        
      if($request->ajax()){
            DB::beginTransaction();
            try{
                /*09 de abril 2020:esta línea se puso en esta posición, ya que se andaban isertando dos registros a la vez
             *como si el request se ejecutara dos veces, estando dentro de este if
             *solo se ejecuta una vez
             *10 de abril: El mensade de flash success no se ejecutaba por lo que no llegaba el response
             *a la solicitud ajax, entonces cuando se manda el success nos dirige a index
             *y de esa forma el mensaje se visualiza
             */
           // $compras = $this->comprasRepository->create($input);
            
           $fecha= $request->input('fecha');
           $id_proveedor= $request->input('proveedor');
           $total= $request->input('total');
           $usuario= Auth::user()->name;

            DB::insert('insert into compras(fecha
            ,id_proveedor
            ,total
            ,usuario
            ,created_at) values (?,?,?,?,?)',[
            $fecha
            ,$id_proveedor
            ,$total
            ,$usuario
            ,new \Datetime()
            ]);
            //es necesario obtener el ultimo id registrado para ingresarlo a la base de datos
            $id_c= DB::select('select last_insert_id() as id_compra');

            //el resultado de la consulta se vacia en un array, accedemos a el con el indice y la clave
            $id_compra = $id_c[0]->id_compra;
            // es posible asignar los datos a un arreglo con un estilo 'clave'=>'valor'
            //$array=[];
            $producto=json_decode( $request->input('producto'));
            $precio=json_decode( $request->input('precio'));
            $cantidad=json_decode( $request->input('cantidad'));
            $subtotal=json_decode( $request->input('subtotal'));
           
                /*
                 * ya que se pueden insertar varios productos a la vez se usa un bucle,
                 * y por cuestiones de diseño de las tablas de base de datos se inserta primero
                 * la compra, enseguida el detalle de compra 
                 */
                for($i=0;$i<sizeof($producto);$i++){
                    
                    $id_pro = DB::select('select id from productos where codigo = ?', [$producto[$i]]);
                    $id_producto = $id_pro[0]->id;

                    DB::select('call sp_ingresarMercancia (?, ?, ?, ?, ?)',
                    [$id_compra,
                    $id_producto,
                    $cantidad[$i],
                    $precio[$i],
                    $subtotal[$i]]);
                }
          
                DB::commit();

                //respondemos a ajax de exito, para 'notificar' que lo que nos envió llegó correctamente
                return response()->json(['exito'=>'exito']);
                Flash::success($this->STORE);
            }catch(\Exception $e){
                Flash::error('Ha ocurrido un problema');
                DB::rollback();
            }
        } 
        
        return redirect(route('compras.index'));
    }

    /**
     * Display the specified Compras.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $compras = $this->comprasRepository->findWithoutFail($id);

        if (empty($compras)) {
            Flash::error($this->ERROR);

            return redirect(route('compras.index'));
        }
        $proveedor = DB::select('select nombre from proveedors where id = (select id_proveedor from compras where id = ?)',[$id]);

        $comprasDetalles = DB::select('call sp_detallesCompra(?)',[$id]);

        return view('compras.show',compact('compras','comprasDetalles','proveedor'));
    }

}

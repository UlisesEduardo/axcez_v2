<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateRegionRequest;
use App\Http\Requests\UpdateRegionRequest;
use App\Repositories\RegionRepository;
use App\Http\Controllers\AppBaseController;
use App\Models\Region;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class RegionController extends AppBaseController
{
    /** @var  RegionRepository */
    private $regionRepository;
    var $STORE;
    var $ERROR;
    var $UPDATE;
    var $DELETE;

    public function __construct(RegionRepository $regionRepo)
    {
        $this->middleware('permission:regions.index')->only('index');
        $this->middleware('permission:regions.create')->only(['create','store']);
        $this->middleware('permission:regions.show')->only('show');
        $this->middleware('permission:regions.edit')->only(['edit','update']);
        $this->middleware('permission:regions.destroy')->only('destroy');
        $this->regionRepository = $regionRepo;
        $this->STORE = 'Datos registrados de forma exitosa';
        $this->ERROR = 'Datos no encontrados';
        $this->UPDATE = 'Datos actualizados de forma exitosa';
        $this->DELETE = 'Registro correctamente eliminado';
    }

    /**
     * Display a listing of the Region.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->regionRepository->pushCriteria(new RequestCriteria($request));
        //$regions = $this->regionRepository->all();
        $regions = Region::paginate(10);
        return view('regions.index')
            ->with('regions', $regions);
    }

    /**
     * Show the form for creating a new Region.
     *
     * @return Response
     */
    public function create()
    {
        return view('regions.create');
    }

    /**
     * Store a newly created Region in storage.
     *
     * @param CreateRegionRequest $request
     *
     * @return Response
     */
    public function store(CreateRegionRequest $request)
    {
        $input = $request->all();

        $region = $this->regionRepository->create($input);

        Flash::success($this->STORE);

        return redirect(route('regions.index'));
    }

    /**
     * Display the specified Region.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $region = $this->regionRepository->findWithoutFail($id);

        if (empty($region)) {
            Flash::error($this->ERROR);

            return redirect(route('regions.index'));
        }

        return view('regions.show')->with('region', $region);
    }

    /**
     * Show the form for editing the specified Region.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $region = $this->regionRepository->findWithoutFail($id);

        if (empty($region)) {
            Flash::error($this->ERROR);

            return redirect(route('regions.index'));
        }

        return view('regions.edit')->with('region', $region);
    }

    /**
     * Update the specified Region in storage.
     *
     * @param  int              $id
     * @param UpdateRegionRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateRegionRequest $request)
    {
        $region = $this->regionRepository->findWithoutFail($id);

        if (empty($region)) {
            Flash::error($this->ERROR);

            return redirect(route('regions.index'));
        }

        $region = $this->regionRepository->update($request->all(), $id);

        Flash::success($this->UPDATE);

        return redirect(route('regions.index'));
    }

    /**
     * Remove the specified Region from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $region = $this->regionRepository->findWithoutFail($id);

        if (empty($region)) {
            Flash::error($this->ERROR);

            return redirect(route('regions.index'));
        }

        $this->regionRepository->delete($id);

        Flash::success($this->DELETE);

        return redirect(route('regions.index'));
    }
}

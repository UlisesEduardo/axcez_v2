<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateContratoRequest;
use App\Http\Requests\UpdateContratoRequest;
use App\Repositories\ContratoRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use App\Models\Sectorial;
use App\Models\Plan;
use Illuminate\Support\Facades\DB;
use \DateTime;
//Datos del clientes
use App\Models\Cliente;
use App\Repositories\ClienteRepository;
use App\Http\Requests\CreateClienteRequest;
use App\Models\Contrato;

class ContratoController extends AppBaseController
{
    /** @var  ContratoRepository */
    private $contratoRepository;
    private $clienteRepository;
    var $STORE;
    var $ERROR;
    var $UPDATE;
    var $DELETE;

    public function __construct(ContratoRepository $contratoRepository, ClienteRepository $clienteRepository)
    {
        $this->middleware('permission:contratos.index')->only('index');
        $this->middleware('permission:contratos.create')->only(['create','store']);
        $this->middleware('permission:contratos.show')->only('show');
        $this->middleware('permission:contratos.edit')->only(['edit','update']);
        $this->middleware('permission:contratos.destroy')->only('destroy');
        $this->contratoRepository = $contratoRepository;
        $this->clienteRepository = $clienteRepository;
        $this->STORE = 'Datos registrados de forma exitosa';
        $this->ERROR = 'Datos no encontrados';
        $this->UPDATE = 'Datos actualizados de forma exitosa';
        $this->DELETE = 'Registro correctamente eliminado';
    }

    /**
     * Display a listing of the Contrato.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->contratoRepository->pushCriteria(new RequestCriteria($request));
        //$contratos = $this->contratoRepository->all();
        $contratos = Contrato::paginate(10);
        return view('contratos.index')
            ->with('contratos', $contratos);
    }

    /**
     * Show the form for creating a new Contrato.
     *
     * @return Response
     */
    public function create()
    {
        $sectorialItems = Sectorial::pluck('nombre', 'id');
        $planItems = Plan::pluck('nombre', 'id');
        $productos = DB::select('select codigo,descipcion,cantidad,precioVenta from productos');
        return view('contratos.create', compact('sectorialItems', 'planItems', 'productos'));
    }

    /**
     * Store a newly created Contrato in storage.
     *
     * @param CreateContratoRequest $request
     *
     * @return Response
     */

    /**
     * Es posible poner en el constructor de la clase dos repositories
     * uno de cliente y otro de contrato (ContratoRepository $contratoRepository...)
     * y hacer el insert con tan sólo dos líneas, pero para control propio lo dejé con
     * el bloque que se ve enseguida
     * 
     */
    public function _store(Request $request)
    {
        if ($request->ajax()) {

            //se obtienen del request los datos del contrato
            $fechaContrato = $request->input('fechaContrato');
            $notasAdicionales = $request->input('notasAdicionales');
            $fechaInstalacion = $request->input('fechaInstalacion');
            $id_sector = $request->input('id_sector');
            $id_plan = $request->input('id_plan');
            $ip = $request->input('ip');
            $gateway = $request->input('gateway');
            $fechaPago = $request->input('fechaPago');
            $montoReal = $request->input('montoReal');
            $montoCobrado = $request->input('montoCobrado');
            $tipoContrato = $request->input('tipoContrato');

            //se obtienen del request los datos del cliente
            $codigoCliente = $request->input('codigoCliente');
            $nombres = $request->input('nombres');
            $apellidoPaterno = $request->input('apellidoPaterno');
            $apellidoMaterno = $request->input('apellidoMaterno');
            $municipio = $request->input('municipio');
            $barrio = $request->input('barrio');
            $calle = $request->input('calle');
            $num = $request->input('num');
            $referencia = $request->input('referencia');
            $coordenadas = $request->input('coordenadas');
            $telefono = $request->input('telefono');
            $telefonoAlternativo = $request->input('telefonoAlternativo');
            $correo = $request->input('correo');

            //del request obtenemos los productos que se vendieron
            $producto = json_decode($request->input('producto'));
            $precio = json_decode($request->input('precio'));
            $cantidad = json_decode($request->input('cantidad'));
            $subtotal = json_decode($request->input('subtotal'));

            DB::beginTransaction();

            try {
                //se insertan los datos del contrato 
                DB::insert('insert into contratos(fechaContrato
            ,notasAdicionales
            ,fechaInstalacion
            ,id_sector
            ,id_plan
            ,ip
            ,gateway
            ,fechaPago
            ,montoReal
            ,montoCobrado
            ,tipoContrato
            ,created_at) values (?,?,?,?,?,?,?,?,?,?,?,?)', [
                    $fechaContrato, $notasAdicionales, $fechaInstalacion, $id_sector, $id_plan, $ip, $gateway, $fechaPago, $montoReal, $montoCobrado, $tipoContrato, new Datetime()
                ]);
                //obtenemos el id del contrato y el folio generado
                $id_c = DB::select('select last_insert_id() as id_contrato');
                $id_contrato = $id_c[0]->id_contrato;
                //se insertan los datos del cliente obtenidos anteriormente, pero verificamos
                //primero si el cliente ya tiene otro contrato anterior o es nuevo
                //si, codigoCliente es null...
                if (!$codigoCliente) {
                    DB::insert(
                        'insert into clientes(nombres
                ,apellidoPaterno
                ,apellidoMaterno
                ,municipio
                ,barrio
                ,calle
                ,num
                ,referencia
                ,coordenadas
                ,telefono
                ,telefonoAlternativo
                ,correo
                ,created_at) values (?,?,?,?,?,?,?,?,?,?,?,?,?)',
                        [
                            $nombres, $apellidoPaterno, $apellidoMaterno, $municipio, $barrio, $calle, $num, $referencia, $coordenadas, $telefono, $telefonoAlternativo, $correo, new Datetime()
                        ]
                    );
                    //obtenemos el id del cliente registrado
                    $id_cli = DB::select('select last_insert_id() as id_cliente');
                } else {
                    //o en su defecto el id que ya tiene asignado anteriormente
                    //para evitar mas variables dejamos la consulta como as id_cliente,
                    //puesto que reusaremos esa variable
                    $id_cli = DB::select(
                        'select id as id_cliente from clientes where codigo = ?',
                        [$codigoCliente]
                    );
                }

                $id_cliente = $id_cli[0]->id_cliente;
                //teniendo la id del cliente y la del contrato las vinculamos en la tabla
                DB::insert('insert into contrato_cliente(id_contrato,id_cliente)
            values(?,?)', [$id_contrato, $id_cliente]);
                //se insertan los detalles del contrato
                for ($i = 0; $i < sizeof($producto); $i++) {

                    $id_pro = DB::select('select id from productos where codigo = ?', [$producto[$i]]);
                    $id_producto = $id_pro[0]->id;

                    DB::select(
                        'call sp_descontarMercancia (?, ?, ?, ?, ?)',
                        [
                            $id_contrato,
                            $id_producto,
                            $cantidad[$i],
                            $precio[$i],
                            $subtotal[$i]
                        ]
                    );
                }
                DB::commit();
                return response()->json(['exito' => 'exito']);
                Flash::success($this->SUCCESS);
            } catch (\Exception $e) {
                Flash::error('Ha ocurrido un problema');
                DB::rollback();
            }
        }

        return redirect(route('contratos.index'));
    }

    /**
     * Display the specified Contrato.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $contrato = $this->contratoRepository->findWithoutFail($id);

        if (empty($contrato)) {
            Flash::error($this->ERROR);

            return redirect(route('contratos.index'));
        }
        $cliente = DB::select('call sp_contratoEditar(?)', [$id]);
        $productos = DB::select('call sp_productoContrato(?)', [$id]);
        return view('contratos.show', compact('contrato', 'cliente', 'productos'));
    }

    /**
     * Show the form for editing the specified Contrato.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $contrato = $this->contratoRepository->findWithoutFail($id);

        if (empty($contrato)) {
            Flash::error($this->ERROR);

            return redirect(route('contratos.index'));
        }

        $cliente = DB::select('call sp_contratoEditar(?)', [$id]);

        return view('contratos.edit', compact('contrato', 'cliente'));
    }

    /**
     * Update the specified Contrato in storage.
     *
     * @param  int              $id
     * @param UpdateContratoRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateContratoRequest $request)
    {
        $contrato = $this->contratoRepository->findWithoutFail($id);

        if (empty($contrato)) {
            Flash::error($this->ERROR);

            return redirect(route('contratos.index'));
        }

        $contrato = $this->contratoRepository->update($request->all(), $id);

        Flash::success($this->UPDATE);

        return redirect(route('contratos.index'));
    }

    /**
     * Remove the specified Contrato from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $contrato = $this->contratoRepository->findWithoutFail($id);

        if (empty($contrato)) {
            Flash::error($this->ERROR);

            return redirect(route('contratos.index'));
        }

        $this->contratoRepository->delete($id);

        Flash::success($this->DELETE);

        return redirect(route('contratos.index'));
    }

    /*
     *Basicamente en este método se encarga de guardar los datos del contrato
     *si fallan las reglas de alguno de los modelos el request nos lo hará saber mediante los 
     *mensajes de validacion, y nos va a retornar la misma vista, es decir el formulario.Ambos 
     *request tienen la misma información, solo que necesitamos incluir ambos request por lo ya 
     *explicado antes, las validaciones, en la parte del ajax que mandaba la informacion por cada input
     *del formulario, se ha comentado en gran parte de el y se han dejado solo los json con los detalles
     *de compra
     *actualizacion: aun comentado parte del ajax sigo recibiendo los datos, pero eso genera
     *un error de validacion del form
     *
     * actualizacion 2: depurando el bloque de codigo me di cuenta que no es el ajax el problema
     *sino que hacia 2 veces el $request->all(), basta con uno, al final de cuenta ambos 
     *tienen la misma info, un request no se usara, solo hara la validacion del modelo

     * actualizacion 3: funciona con el bloque del store anterior pero debe mejorar
     */
     //Emviar los con ajax atributos son opcionales ya que el request usado es el de la clase, CreateContratoRequest
    //CreateClienteRequest, no es necesario mandarlos
    public function store(CreateContratoRequest $request, CreateClienteRequest $requestC)
    {
        $input = $request->all();
        $codigoCliente = $request->input('codigoCliente');

        //del request obtenemos los productos que se vendieron
        $producto = json_decode($request->input('producto'));
        $precio = json_decode($request->input('precio'));
        $cantidad = json_decode($request->input('cantidad'));
        $subtotal = json_decode($request->input('subtotal'));

        DB::beginTransaction();

        try {
            //se insertan los datos del contrato 
            $contrato = $this->contratoRepository->create($input);
            //obtenemos el id del contrato y el folio generado
            $id_c = DB::select('select last_insert_id() as id_contrato');
            $id_contrato = $id_c[0]->id_contrato;
            //se insertan los datos del cliente obtenidos anteriormente, pero verificamos
            //primero si el cliente ya tiene otro contrato anterior o es nuevo
            //si, codigoCliente es null...
            if (!$codigoCliente) {
                $cliente = $this->clienteRepository->create($input);
                //obtenemos el id del cliente registrado
                $id_cli = DB::select('select last_insert_id() as id_cliente');
            } else {
                //o en su defecto el id que ya tiene asignado anteriormente
                //para evitar mas variables dejamos la consulta como as id_cliente,
                //puesto que reusaremos esa variable
                $id_cli = DB::select(
                    'select id as id_cliente from clientes where codigo = ?',
                    [$codigoCliente]
                );
            }

            $id_cliente = $id_cli[0]->id_cliente;
            //teniendo la id del cliente y la del contrato las vinculamos en la tabla
            DB::insert('insert into contrato_cliente(id_contrato,id_cliente)
            values(?,?)', [$id_contrato, $id_cliente]);
            //se insertan los detalles del contrato
            for ($i = 0; $i < sizeof($producto); $i++) {

                $id_pro = DB::select('select id from productos where codigo = ?', [$producto[$i]]);
                $id_producto = $id_pro[0]->id;

                DB::select(
                    'call sp_descontarMercancia (?, ?, ?, ?, ?)',
                    [
                        $id_contrato,
                        $id_producto,
                        $cantidad[$i],
                        $precio[$i],
                        $subtotal[$i]
                    ]
                );
            }
            DB::commit();
            return response()->json(['exito'=>'exito']);
        } catch (\Exception $e) {
            //Flash::error('Ha ocurrido un problema');
            DB::rollback();
        }
        Flash::success($this->STORE);
        return redirect(route('contratos.index'));
    }
}

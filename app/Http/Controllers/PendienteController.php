<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatePendienteRequest;
use App\Http\Requests\UpdatePendienteRequest;
use App\Repositories\PendienteRepository;
use App\Http\Controllers\AppBaseController;
use App\Models\Pendiente;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class PendienteController extends AppBaseController
{
    /** @var  PendienteRepository */
    private $pendienteRepository;
    var $STORE;
    var $ERROR;
    var $UPDATE;
    var $DELETE;

    public function __construct(PendienteRepository $pendienteRepo)
    {
        $this->middleware('permission:pendientes.index')->only('index');
        $this->middleware('permission:pendientes.create')->only(['create','store']);
        $this->middleware('permission:pendientes.show')->only('show');
        $this->middleware('permission:pendientes.edit')->only(['edit','update']);
        $this->middleware('permission:pendientes.destroy')->only('destroy');
        $this->pendienteRepository = $pendienteRepo;
        $this->STORE = 'Datos registrados de forma exitosa';
        $this->ERROR = 'Datos no encontrados';
        $this->UPDATE = 'Datos actualizados de forma exitosa';
        $this->DELETE = 'Registro correctamente eliminado';
    }

    /**
     * Display a listing of the Pendiente.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->pendienteRepository->pushCriteria(new RequestCriteria($request));
        //$pendientes = $this->pendienteRepository->all();
        $pendientes = Pendiente::paginate(10);
        return view('pendientes.index')
            ->with('pendientes', $pendientes);
    }

    /**
     * Show the form for creating a new Pendiente.
     *
     * @return Response
     */
    public function create()
    {
        return view('pendientes.create');
    }

    /**
     * Store a newly created Pendiente in storage.
     *
     * @param CreatePendienteRequest $request
     *
     * @return Response
     */
    public function store(CreatePendienteRequest $request)
    {
        $input = $request->all();

        $codigoCliente = $request->input('codigoCliente');

        $cliente = DB::select('select id from clientes where codigo = ?', [$codigoCliente]);

        if (empty($cliente)) {
            Flash::error($this->ERROR);

            return redirect(route('pendientes.index'));
        }

        $input['usuario'] = Auth::user()->name;
        $pendiente = $this->pendienteRepository->create($input);

        Flash::success($this->STORE);

        return redirect(route('pendientes.index'));
    }

    /**
     * Display the specified Pendiente.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $pendiente = $this->pendienteRepository->findWithoutFail($id);

        if (empty($pendiente)) {
            Flash::error($this->ERROR);

            return redirect(route('pendientes.index'));
        }

        return view('pendientes.show')->with('pendiente', $pendiente);
    }

    /**
     * Show the form for editing the specified Pendiente.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $pendiente = $this->pendienteRepository->findWithoutFail($id);

        if (empty($pendiente)) {
            Flash::error($this->ERROR);

            return redirect(route('pendientes.index'));
        }

        return view('pendientes.edit')->with('pendiente', $pendiente);
    }

    /**
     * Update the specified Pendiente in storage.
     *
     * @param  int              $id
     * @param UpdatePendienteRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePendienteRequest $request)
    {
        $pendiente = $this->pendienteRepository->findWithoutFail($id);

        if (empty($pendiente)) {
            Flash::error($this->ERROR);

            return redirect(route('pendientes.index'));
        }

        $pendiente = $this->pendienteRepository->update($request->all(), $id);

        Flash::success($this->UPDATE);

        return redirect(route('pendientes.index'));
    }

    /**
     * Remove the specified Pendiente from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $pendiente = $this->pendienteRepository->findWithoutFail($id);

        if (empty($pendiente)) {
            Flash::error($this->ERROR);

            return redirect(route('pendientes.index'));
        }

        $this->pendienteRepository->delete($id);

        Flash::success($this->DELETE);

        return redirect(route('pendientes.index'));
    }
}

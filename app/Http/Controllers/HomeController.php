<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Laracasts\Flash\Flash;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $egresosCompras = DB::select('call sp_egresosCompras()');
        $egresosGastos = DB::select('call sp_egresosGastos()');
        $ingresosContratos = DB::select('call sp_ingresosContratos()');
        $ingresosPagos = DB::select('call sp_ingresosPagos()');
        $clientes = DB::select('select count(id) as clientes from clientes');
        $condonados = DB::select('call sp_condonados()');
        $pendientes = DB::select('call sp_pendientes()');
        $productos = DB::select('call sp_productos()');
        $pagos = DB::select('call sp_pagos()');
        $planes = DB::select('call sp_planes()');
        $sectoriales = DB::select('call sp_sectoriales()');
        $totalCompras = DB::select('select sum(total) as totalCompras from compras');
        $totalGastos = DB::select('select sum(total) as totalGastos from gastos');
        $totalContratos = DB::select('select sum(montoCobrado) as totalContratos from contratos');
        $totalPagos = DB::select('select sum(importe) as totalPagos from pagos where condonado is null');
        $totalCondonados = DB::select('select sum(importe) as totalCondonados from pagos where condonado=1');
        return view('home',compact('egresosCompras'
                                    ,'egresosGastos'
                                    ,'ingresosContratos'
                                    ,'ingresosPagos' 
                                    ,'clientes'
                                    ,'condonados'
                                    ,'pendientes'
                                    ,'productos'
                                    ,'pagos'
                                    ,'planes'
                                    ,'sectoriales'
                                    ,'totalCompras'
                                    ,'totalGastos'
                                    ,'totalContratos'
                                    ,'totalPagos'
                                    ,'totalCondonados'
                                ));
    }
}

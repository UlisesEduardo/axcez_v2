<?php

namespace App\Providers;
use App\Models\Plan;
use App\Models\Sectorial;
use App\Models\Region;

use Illuminate\Support\ServiceProvider;
use View;

class ViewServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer(['pagos.fields'], function ($view) {
            $planItems = Plan::pluck('nombre','id')->toArray();
            $view->with('planItems', $planItems);
        });
        View::composer(['contratos.fields'], function ($view) {
            $planItems = Plan::pluck('nombre','id')->toArray();
            $view->with('planItems', $planItems);
        });
        View::composer(['contratos.fields'], function ($view) {
            $sectorialItems = Sectorial::pluck('nombre','id')->toArray();
            $view->with('sectorialItems', $sectorialItems);
        });
        View::composer(['contratos.fields'], function ($view) {
            $planItems = Plan::pluck('nombre','id')->toArray();
            $view->with('planItems', $planItems);
        });
        View::composer(['contratos.fields'], function ($view) {
            $sectorialItems = Sectorial::pluck('nombre','id')->toArray();
            $view->with('sectorialItems', $sectorialItems);
        });
        View::composer(['sectorials.fields'], function ($view) {
            $regionItems = Region::pluck('nombreRegion','id')->toArray();
            $view->with('regionItems', $regionItems);
        });
        View::composer(['plans.fields'], function ($view) {
            $regionItems = Region::pluck('nombreRegion','id')->toArray();
            $view->with('regionItems', $regionItems);
        });
        //
    }
}
<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePendientesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pendientes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('codigoCliente');
            $table->string('motivo');
            $table->date('fechaProgramada');
            $table->string('estatus');
            $table->string('urgencia');
            $table->string('velocidadRed');
            $table->string('notas');
            $table->string('usuario');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pendientes');
    }
}

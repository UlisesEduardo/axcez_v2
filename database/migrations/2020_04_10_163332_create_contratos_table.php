<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateContratosTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contratos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('folio');
            $table->date('fechaContrato');
            $table->string('notasAdicionales');
            $table->date('fechaInstalacion');
            $table->integer('id_sector')->unsigned();
            $table->integer('id_plan')->unsigned();
            $table->string('ip');
            $table->string('gateway');
            $table->string('fechaPago');
            $table->double('montoReal');
            $table->double('montoCobrado');
            $table->string('tipoContrato');
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('id_sector')->references('id')->on('sectorials');
            $table->foreign('id_plan')->references('id')->on('plans');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('contratos');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePlansTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plans', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_region')->unsigned();
            $table->string('nombre');
            $table->string('subida');
            $table->string('bajada');
            $table->double('precio');
            $table->double('precioMin');
            $table->double('precioMax');
            $table->string('descripcion');
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('id_region')->references('id')->on('regions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('plans');
    }
}

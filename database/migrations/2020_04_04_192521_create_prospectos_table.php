<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProspectosTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prospectos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombres');
            $table->string('apellidoPaterno');
            $table->string('apellidoMaterno');
            $table->string('municipio');
            $table->string('barrio');
            $table->string('calle');
            $table->string('num');
            $table->string('referencia');
            $table->string('coordenadas');
            $table->string('telefono');
            $table->string('telefonoAlternativo');
            $table->string('correo');
            $table->string('notas');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('prospectos');
    }
}

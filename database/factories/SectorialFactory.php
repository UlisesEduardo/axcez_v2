<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Sectorial;
use Faker\Generator as Faker;

$factory->define(Sectorial::class, function (Faker $faker) {

    return [
        'id_region' => $faker->randomDigitNotNull,
        'nombre' => $faker->word,
        'ip' => $faker->word,
        'gateway' => $faker->word,
        'notas' => $faker->word,
        'frecuencia' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});

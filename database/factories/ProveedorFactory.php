<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Proveedor;
use Faker\Generator as Faker;

$factory->define(Proveedor::class, function (Faker $faker) {

    return [
        'nombre' => $faker->word,
        'telefonoContacto' => $faker->word,
        'telefonoSecundario' => $faker->word,
        'correo' => $faker->word,
        'encargado' => $faker->word,
        'ubicacion' => $faker->word,
        'pagina' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});

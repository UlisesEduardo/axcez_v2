<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Gastos;
use Faker\Generator as Faker;

$factory->define(Gastos::class, function (Faker $faker) {

    return [
        'concepto' => $faker->word,
        'total' => $faker->word,
        'fecha' => $faker->word,
        'usuario' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});

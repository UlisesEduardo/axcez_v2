<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Cliente;
use Faker\Generator as Faker;

$factory->define(Cliente::class, function (Faker $faker) {

    return [
        'codigo' => $faker->word,
        'nombres' => $faker->word,
        'apellidoPaterno' => $faker->word,
        'apellidoMaterno' => $faker->word,
        'municipio' => $faker->word,
        'barrio' => $faker->word,
        'calle' => $faker->word,
        'num' => $faker->word,
        'referencia' => $faker->word,
        'coordenadas' => $faker->word,
        'telefono' => $faker->word,
        'telefonoAlternativo' => $faker->word,
        'correo' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});

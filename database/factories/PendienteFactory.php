<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Pendiente;
use Faker\Generator as Faker;

$factory->define(Pendiente::class, function (Faker $faker) {

    return [
        'codigoCliente' => $faker->word,
        'motivo' => $faker->word,
        'fechaProgramada' => $faker->word,
        'estatus' => $faker->word,
        'urgencia' => $faker->word,
        'velocidadRed' => $faker->word,
        'notas' => $faker->word,
        'usuario' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});

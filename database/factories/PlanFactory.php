<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Plan;
use Faker\Generator as Faker;

$factory->define(Plan::class, function (Faker $faker) {

    return [
        'id_region' => $faker->randomDigitNotNull,
        'nombre' => $faker->word,
        'subida' => $faker->word,
        'bajada' => $faker->word,
        'precio' => $faker->word,
        'precioMin' => $faker->word,
        'precioMax' => $faker->word,
        'descripcion' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});

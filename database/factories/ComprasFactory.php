<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Compras;
use Faker\Generator as Faker;

$factory->define(Compras::class, function (Faker $faker) {

    return [
        'fecha' => $faker->word,
        'total' => $faker->word,
        'proveedor' => $faker->word,
        'usuario' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});

<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Contrato;
use Faker\Generator as Faker;

$factory->define(Contrato::class, function (Faker $faker) {

    return [
        'folio' => $faker->word,
        'fechaContrato' => $faker->word,
        'notasAdicionales' => $faker->word,
        'fechaInstalacion' => $faker->word,
        'id_sector' => $faker->randomDigitNotNull,
        'id_plan' => $faker->randomDigitNotNull,
        'ip' => $faker->word,
        'gateway' => $faker->word,
        'fechaPago' => $faker->word,
        'montoReal' => $faker->word,
        'montoCobrado' => $faker->word,
        'tipoContrato' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});

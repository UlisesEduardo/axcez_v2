<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Pago;
use Faker\Generator as Faker;

$factory->define(Pago::class, function (Faker $faker) {

    return [
        'importe' => $faker->word,
        'proximoPago' => $faker->word,
        'fechaInicio' => $faker->word,
        'fechaFin' => $faker->word,
        'notas' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});

$(document).ready(function () {
    // Asociar un evento al botón que muestra la ventana modal
    $('#ver-clientes').click(function () {
        var nombres = $("#nombresCliente").val();
        $.ajax({
            // la URL para la petición
            url: '/helpers/buscar',

            // la información a enviar
            data: { 'nombres': nombres },

            // especifica si será una petición POST o GET
            type: 'GET',

            // el tipo de información que se espera de respuesta
            dataType: 'json',

            // código a ejecutar si la petición es satisfactoria;
            success: function (respuesta) {
                //$('#MyProducto').html(respuesta);
                //alert(respuesta);
                // console.log(respuesta);
                $("#tablaClientes").html("");
                for (var i = 0; i < respuesta.length; i++) {
                    var tr = `<tr>
                <td>`+ respuesta[i].codigo + `</td>
                <td>`+ respuesta[i].nombres + `</td>
                <td>`+ respuesta[i].apellidoPaterno + `</td>
                <td>`+ respuesta[i].apellidoMaterno + `</td>
              </tr>`;
                    $("#tablaClientes").append(tr)
                }
                $('#modalClientes').modal('show');
            },

            // código a ejecutar si la petición falla;
            error: function (xhr, status) {
                alert('No se hallaron clientes con esos datos');
            },
        });
    });
});


function calcularSubtotal() {
    var cantidad = $("#cantidad").val();
    var precio = $("#precio").val();
    $("#subtotal").val(cantidad * precio);
}



$(document).ready(function () {
    var id = 1;
    $("#add").click(function () {
        var codigo = $("#codigoProducto").val();
        var cantidad = $("#cantidad").val();
        var precio = $("#precio").val();
        // var decimal = /^[0-9]+([.][0-9]+)?$/; //Expresión regular para aceptar solo números decimales
        var entero = /^\d*$/; //Expresión regular para aceptar solo números enteros positivos
        $.ajax({
            // la URL para la petición
            url: '/helpers/buscarProducto',

            // la información a enviar
            data: { 'codigo': codigo },

            // especifica si será una petición POST o GET
            type: 'GET',

            // el tipo de información que se espera de respuesta
            dataType: 'json',

            // código a ejecutar si la petición es satisfactoria;
            success: function (respuesta) {
                if (isNaN(precio) || precio <= 0) {
                    errorPrecio.innerHTML = "Ingrese un precio correcto.";
                } else if (!entero.test(cantidad) || cantidad <= 0) {
                    errorCantidad.innerHTML = "Ingrese una cantidad correcta.";
                } else {
                    var newid = id++;
                    $("#tableProductos").append('<tr valign="top" id="' + newid + '">\n\
                    <td width="100px" >' + newid + '</td>\n\
                    <td width="100px" class="codigoProducto'+ newid + '">' + $("#codigoProducto").val() + '</td>\n\
                    <td width="100px" class="cantidad'+ newid + '">' + $("#cantidad").val() + '</td>\n\
                    <td width="100px" class="precio'+ newid + '">' + $("#precio").val() + '</td>\n\
                    <td width="100px" class="subtotal'+ newid + '">' + $("#subtotal").val() + '</td>\n\
                    <td width="100px"><a href="javascript:void(0);"  class="remCF">Quitar</a></td>\n\
                </tr>');

                    $('#codigoProducto').val('');
                    $('#cantidad').val('');
                    $('#precio').val('');
                    $('#subtotal').val('');

                    calcularTotal();
                    errorPrecio.innerHTML = " ";
                    errorCantidad.innerHTML = " ";
                    errorCodigo.innerHTML = " ";
                }
            },

            // código a ejecutar si la petición falla;
            error: function (xhr, status) {
                errorCodigo.innerHTML = "El artículo no esta registrado.";
            },
        });

    });
});


function calcularTotal() {
    var total = 0;
    $("#tableProductos tr").find('td:eq(4)').each(function () {
        valor = $(this).html();
        total += parseFloat(valor);
    });
    $("#total").val(total);
}


$("#tableProductos").on('click', '.remCF', function () {
    $(this).parent().parent().remove();
    calcularTotal();
});



$("#save").click(function () {
    var lastRowId = $('#tableProductos tr:last').attr("id");
    var url = $(this).attr("data-url");
    var producto = new Array();
    var cantidad = new Array();
    var precio = new Array();
    var subtotal = new Array();
    var fecha = $("#fecha").val();
    var total = $("#total").val();
    var proveedor = $("#proveedor option:selected").val();
    var token = $('#token').val();
    for (var i = 1; i <= lastRowId; i++) {
        producto.push($("#" + i + " .codigoProducto" + i).html());
        cantidad.push($("#" + i + " .cantidad" + i).html());
        precio.push($("#" + i + " .precio" + i).html());
        subtotal.push($("#" + i + " .subtotal" + i).html());
    }
    var sendProducto = JSON.stringify(producto);
    var sendCantidad = JSON.stringify(cantidad);
    var sendPrecio = JSON.stringify(precio);
    var sendSubtotal = JSON.stringify(subtotal);
    $.ajax({
        url: url,
        headers: { 'X-CSRF-TOKEN': token },
        type: "post",
        dataType: "json",
        data: {
            fecha: fecha
            , total: total
            , proveedor: proveedor
            , token: token
            , producto: sendProducto
            , cantidad: sendCantidad
            , precio: sendPrecio
            , subtotal: sendSubtotal
        },
        success: function (data) {
            alert("Registro almacenado de forma exitosa");
        }
    });
});


$("#guardarContrato").click(function () {
    var filaId = $('#tableProductos tr:last').attr("id");
    var url = $(this).attr("contrato-url");
    var token = $('#token').val();
    var producto = new Array();
    var cantidad = new Array();
    var precio = new Array();
    var subtotal = new Array();
    for (var i = 1; i <= filaId; i++) {
        producto.push($("#" + i + " .codigoProducto" + i).html());
        cantidad.push($("#" + i + " .cantidad" + i).html());
        precio.push($("#" + i + " .precio" + i).html());
        subtotal.push($("#" + i + " .subtotal" + i).html());
    }
    var sendProducto = JSON.stringify(producto);
    var sendCantidad = JSON.stringify(cantidad);
    var sendPrecio = JSON.stringify(precio);
    var sendSubtotal = JSON.stringify(subtotal);
    $.ajax({
        url: url,
        headers: { 'X-CSRF-TOKEN': token },
        type: "post",

        dataType: "json",
        data: {
            producto: sendProducto
            , cantidad: sendCantidad
            , precio: sendPrecio
            , subtotal: sendSubtotal
            //Estos atributos son opcionales ya que el request usado es el de la clase, CreateContratoRequest
            //CreateClienteRequest, no es necesario mandarlos
            , fechaContrato: $("#fechaContrato").val()
            , notasAdicionales: $("#notasAdicionales").val()
            , fechaInstalacion: $("#fechaInstalacion").val()
            , id_sector: $("#id_sector option:selected").val()
            , id_plan: $("#id_plan option:selected").val()
            , ip: $("#ip").val()
            , gateway: $("#gateway").val()
            , fechaPago: $("#fechaPago option:selected").text()
            , montoReal: $("#montoReal").val()
            , montoCobrado: $("#montoCobrado").val()
            , tipoContrato: $("#tipoContrato option:selected").text()
            , codigoCliente: $('#codigo').val()
            , nombres: $("#nombres").val()
            , apellidoPaterno: $("#apellidoPaterno").val()
            , apellidoMaterno: $("#apellidoMaterno").val()
            , municipio: $("#municipio").val()
            , barrio: $("#barrio").val()
            , calle: $("#calle").val()
            , num: $("#num").val()
            , referencia: $("#referencia").val()
            , coordenadas: $("#coordenadas").val()
            , telefono: $("#telefono").val()
            , telefonoAlternativo: $("#telefonoAlternativo").val()
            , correo: $("#correo").val()
        },
        success: function (data) {
            //alert("Registro almacenado de forma exitosa");
        },
        error: function (xhr, status) {
            //alert("Ha ocurrido un problema");
            //window.location.replace("http://127.0.0.1:8000/contratos/index");
        },
    });
});


$(document).ready(function () {
    var id = 1;
    $("#agregar").click(function () {
        var codigoProducto = $("#codigoProducto").val();
        var cantidadSolicitada = $("#cantidad").val();
        var precio = $("#precio").val();
        $.ajax({
            // la URL para la petición
            url: '/helpers/verificarExistencias',

            // la información a enviar
            data: {
                codigoProducto: codigoProducto
                , cantidadSolicitada: cantidadSolicitada
            },

            // especifica si será una petición POST o GET
            type: 'GET',

            // el tipo de información que se espera de respuesta
            dataType: 'json',

            // código a ejecutar si la petición es satisfactoria;
            success: function (respuesta) {
                if (isNaN(precio) || precio <= 0) {
                    errorPrecio.innerHTML = "Ingrese un precio correcto.";
                } else {
                    var newid = id++;

                    $("#tableProductos").append('<tr valign="top" id="' + newid + '">\n\
                        <td width="100px" >' + newid + '</td>\n\
                        <td width="100px" class="codigoProducto'+ newid + '">' + $("#codigoProducto").val() + '</td>\n\
                        <td width="100px" class="cantidad'+ newid + '">' + $("#cantidad").val() + '</td>\n\
                        <td width="100px" class="precio'+ newid + '">' + $("#precio").val() + '</td>\n\
                        <td width="100px" class="subtotal'+ newid + '">' + $("#subtotal").val() + '</td>\n\
                        <td width="100px"><a href="javascript:void(0);"  class="remCF">Quitar</a></td>\n\
                    </tr>');

                    $('#codigoProducto').val('');
                    $('#cantidad').val('');
                    $('#precio').val('');
                    $('#subtotal').val('');

                    calcularTotal();
                    errorPrecio.innerHTML = " ";
                }

            },

            // código a ejecutar si la petición falla;
            error: function (xhr, status) {
                alert('No hay suficientes unidades o el producto no existe');
            },
        });

    });
});


$(document).ready(function () {
    // Asociar un evento al botón que muestra la ventana modal
    $('#ver-contrato').click(function () {
        var nombres = $("#nombre_cliente").val();
        $.ajax({
            // la URL para la petición
            url: '/helpers/buscarContrato',

            // la información a enviar
            data: { 'nombres': nombres },

            // especifica si será una petición POST o GET
            type: 'GET',

            // el tipo de información que se espera de respuesta
            dataType: 'json',

            // código a ejecutar si la petición es satisfactoria;
            success: function (respuesta) {
                //$('#MyProducto').html(respuesta);
                //alert(respuesta);
                // console.log(respuesta);
                $("#tablaContratos").html("");
                for (var i = 0; i < respuesta.length; i++) {
                    var tr = `<tr>
                  <td>`+ respuesta[i].nombre + `</td>
                  <td>`+ respuesta[i].barrio + `</td>
                  <td>`+ respuesta[i].calle + `</td>
                  <td>`+ respuesta[i].num + `</td>
                  <td>`+ respuesta[i].telefono + `</td>
                  <td>`+ respuesta[i].folio + `</td>
                </tr>`;
                    $("#tablaContratos").append(tr)
                }
                $('#modalContratos').modal('show');
            },

            // código a ejecutar si la petición falla;
            error: function (xhr, status) {
                alert('No se hallaron datos relacionados al cliente');
            },
        });
    });
});


$("#guardarPago").click(function () {
    var url = $(this).attr("pagos-url");
    var token = $('#token').val();
    $.ajax({
        url: url,
        headers: { 'X-CSRF-TOKEN': token },
        type: "post",
        dataType: "json",
        data: {
            folioContrato: $('#folioContrato').val()
            , importe: $('#importe').val()
            , proximoPago: $('#proximoPago').val()
            , fechaInicio: $('#fechaInicio').val()
            , fechaFin: $('#fechaFin').val()
            , condonado: $("#condonado:checked").val()
            , notas: $('#notas').val()
        },
        success: function (data) {
            alert("Registro almacenado de forma exitosa");
        }
    });
});


$(document).ready(function () {
    // Asociar un evento al botón que muestra la ventana modal
    $('#buscarCliente').click(function () {
        var codigoCliente = $("#codigoCliente").val();
        $.ajax({
            // la URL para la petición
            url: '/helpers/buscarFolio',

            // la información a enviar
            data: { 'codigoCliente': codigoCliente },

            // especifica si será una petición POST o GET
            type: 'GET',

            // el tipo de información que se espera de respuesta
            dataType: 'json',

            // código a ejecutar si la petición es satisfactoria;
            success: function (respuesta) {
                $('#codigo').val(respuesta[0].codigo);
                $('#nombres').val(respuesta[0].nombres);
                $("#apellidoPaterno").val(respuesta[0].apellidoPaterno);
                $("#apellidoMaterno").val(respuesta[0].apellidoMaterno);
                $("#municipio").val(respuesta[0].municipio);
                $("#barrio").val(respuesta[0].barrio);
                $("#calle").val(respuesta[0].calle);
                $("#num").val(respuesta[0].num);
                $("#referencia").val(respuesta[0].referencia);
                $("#coordenadas").val(respuesta[0].coordenadas);
                $("#telefono").val(respuesta[0].telefono);
                $("#telefonoAlternativo").val(respuesta[0].telefonoAlternativo);
                $("#correo").val(respuesta[0].correo);
            },

            // código a ejecutar si la petición falla;
            error: function (xhr, status) {
                alert('No se halló el folio ingresado, verifique nuevamente');
            },
        });
    });
});

$('#id_sector').on('change', function () {
    var id_sector = $("#id_sector option:selected").val();

    $.ajax({
        // la URL para la petición
        url: '/helpers/buscarSector',

        // la información a enviar
        data: { 'id_sector': id_sector },

        // especifica si será una petición POST o GET
        type: 'GET',

        // el tipo de información que se espera de respuesta
        dataType: 'json',

        // código a ejecutar si la petición es satisfactoria;
        success: function (respuesta) {
            $('#ip').val(respuesta[0].ip);
            $('#gateway').val(respuesta[0].gateway);
        },

        // código a ejecutar si la petición falla;
        error: function (xhr, status) {
            alert('No se hallaron los datos, verifique nuevamente');
        },
    });
})

$(document).ready(function () {
    // Asociar un evento al botón que muestra la ventana modal
    $('#buscarDatos').click(function () {
        var url = $(this).attr("url");
        var nombres = $("#datosCliente").val();
        $.ajax({
            // la URL para la petición
            url: url,

            // la información a enviar
            data: { 'datosCliente': nombres },

            // especifica si será una petición POST o GET
            type: 'GET',

            // el tipo de información que se espera de respuesta
            dataType: 'json',

            // código a ejecutar si la petición es satisfactoria;
            success: function (respuesta) {
                $('#principalPanel').empty().append($(respuesta));
            },

            // código a ejecutar si la petición falla;
            error: function (respuesta) {

                var errors = respuesta.responseJSON;
                if (errors) {
                    $.each(errors, function (i) {
                        console.log(errors[i]);
                    });
                }
                alert('No se hallaron datos relacionados a ese cliente');
            },
        });
    });
});

$(document).ready(function () {
    setTimeout(function () {
        $("#msg").fadeOut(1500);
    }, 3000);
});
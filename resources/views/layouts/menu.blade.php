<li class="active treeview menu-open">
    <a href="#">
        <i class="icofont-lock"></i> <span>Acceso al sistema</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
        @can('users.index')
        <li class="{{ Request::is('users*') ? 'active' : '' }}">
            <a href="{{ route('users.index') }}"><i class="icofont-user-alt-1"></i><span>Usuarios</span></a>
        </li>
        @endcan
        @can('roles.index')
        <li class="{{ Request::is('roles*') ? 'active' : '' }}">
            <a href="{{ route('roles.index') }}"><i class="icofont-checked"></i><span>Roles y permisos</span></a>
        </li>
        @endcan
    </ul>
</li>

<li class="active treeview menu-open">
    <a href="#">
        <i class="icofont-world"></i> <span>Infraestructura</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
        @can('regions.index')
        <li class="{{ Request::is('regions*') ? 'active' : '' }}">
            <a href="{{ route('regions.index') }}"><i class="icofont-location-pin"></i><span>Regiones</span></a>
        </li>
        @endcan
        @can('sectorials.index')
        <li class="{{ Request::is('sectorials*') ? 'active' : '' }}">
            <a href="{{ route('sectorials.index') }}"><i class="icofont-network-tower"></i><span>Sectoriales</span></a>
        </li>
        @endcan
        @can('plans.index')
        <li class="{{ Request::is('plans*') ? 'active' : '' }}">
            <a href="{{ route('plans.index') }}"><i class="icofont-wifi"></i><span>Planes</span></a>
        </li>
        @endcan
    </ul>
</li>

<li class="active treeview menu-open">
    <a href="#">
        <i class="icofont-shopify"></i> <span>Mercancía</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
        @can('proveedors.index')
        <li class="{{ Request::is('proveedors*') ? 'active' : '' }}">
            <a href="{{ route('proveedors.index') }}"><i class="icofont-workers-group"></i><span>Proveedores</span></a>
        </li>
        @endcan
        @can('productos.index')
        <li class="{{ Request::is('productos*') ? 'active' : '' }}">
            <a href="{{ route('productos.index') }}"><i class="icofont-wifi-router"></i><span>Productos</span></a>
        </li>
        @endcan
        @can('compras.index')
        <li class="{{ Request::is('compras*') ? 'active' : '' }}">
            <a href="{{ route('compras.index') }}"><i class="icofont-cart-alt"></i><span>Compras</span></a>
        </li>
        @endcan
    </ul>
</li>

<li class="active treeview menu-open">
    <a href="#">
        <i class="icofont-money-bag"></i><span>Ingresos</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
        @can('contratos.index')
        <li class="{{ Request::is('contratos*') ? 'active' : '' }}">
            <a href="{{ route('contratos.index') }}"><i class="fa fa-edit"></i><span>Contratos</span></a>
        </li>
        @endcan
        @can('pagos.index')
        <li class="{{ Request::is('pagos*') ? 'active' : '' }}">
            <a href="{{ route('pagos.index') }}"><i class="icofont-dollar-plus"></i><span>Pagos</span></a>
        </li>
        @endcan
    </ul>
</li>
@can('prospectos.index')
<li class="{{ Request::is('prospectos*') ? 'active' : '' }}">
    <a href="{{ route('prospectos.index') }}"><i class="icofont-search-user"></i><span>Prospectos</span></a>
</li>
@endcan

@can('gastos.index')
<li class="{{ Request::is('gastos*') ? 'active' : '' }}">
    <a href="{{ route('gastos.index') }}"><i class="icofont-dollar-minus"></i><span>Gastos</span></a>
</li>
@endcan

@can('clientes.index')
<li class="{{ Request::is('clientes*') ? 'active' : '' }}">
    <a href="{{ route('clientes.index') }}"><i class="icofont-users-social"></i><span>Clientes</span></a>
</li>
@endcan

@can('pendientes.index')
<li class="{{ Request::is('pendientes*') ? 'active' : '' }}">
    <a href="{{ route('pendientes.index') }}"><i class="icofont-paperclip"></i><span>Pendientes</span></a>
</li>
@endcan
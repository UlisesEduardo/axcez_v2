<style>
    table {
        font-family: Arial, Helvetica, sans-serif;
        border-collapse: collapse;
        width: 100%;
    }

    td,
    th {

        border: 1px solid #DDDDDD;
        text-align: left;
        padding: 8px;

    }

    tr:nth-child(even) {
        background-color: #DDDDDD;
    }
</style>

<h2 style="font-family: Arial, Helvetica, sans-serif">
    Informe diario del dia {{ now()->format('d/m/Y')}}
</h2><br>

<h3 style="font-family: Arial, Helvetica, sans-serif" align="center">
    Gastos
</h3>

<table>
    <thead>
        <tr>
            <th>Concepto</th>
            <th>Total</th>
        </tr>
    </thead>
    <tbody>
        @foreach($gastos as $gasto)
        <tr>
            <td>{{ $gasto->concepto }}</td>
            <td>{{ $gasto->total }}</td>
        </tr>
        @endforeach
    </tbody>
</table>
<br>

<h3 style="font-family: Arial, Helvetica, sans-serif" align="center">
    Compras
</h3>

<table>
    <thead>
        <tr>
            <th>Código de producto</th>
            <th>Cantidad comprada</th>
            <th>Precio de compra</th>
            <th>Subtotal</th>
        </tr>
    </thead>
    <tbody>
        @foreach($compras as $compra)
        <tr>
            <td>{{ $compra->codigo }}</td>
            <td>{{ $compra->cantidad }}</td>
            <td>{{ $compra->precioCompra }}</td>
            <td>{{ $compra->subtotal }}</td>
        </tr>
        @endforeach
    </tbody>
</table>
<br>

<h3 style="font-family: Arial, Helvetica, sans-serif" align="center">
    Pagos condonados
</h3>

<table>
    <thead>
        <tr>
            <th>Folio del contrato</th>
            <th>Importe</th>
            <th>Pagado del</th>
            <th>Al</th>
        </tr>
    </thead>
    <tbody>
        @foreach($condonados as $condonado)
        <tr>
            <td>{{ $condonado->folio }}</td>
            <td>{{ $condonado->importe }}</td>
            <td>{{ $condonado->fechaInicio }}</td>
            <td>{{ $condonado->fechaFin }}</td>
        </tr>
        @endforeach
    </tbody>
</table>
<br>

<h3 style="font-family: Arial, Helvetica, sans-serif" align="center">
    Pagos cobrados
</h3>

<table>
    <thead>
        <tr>
            <th>Folio del contrato</th>
            <th>Importe</th>
            <th>Pagado del</th>
            <th>Al</th>
        </tr>
    </thead>
    <tbody>
        @foreach($pagos as $pago)
        <tr>
            <td>{{ $pago->folio }}</td>
            <td>{{ $pago->importe }}</td>
            <td>{{ $pago->fechaInicio }}</td>
            <td>{{ $pago->fechaFin }}</td>
        </tr>
        @endforeach
    </tbody>
</table>
<br>

<h3 style="font-family: Arial, Helvetica, sans-serif" align="center">
    Contratos realizados
</h3>

<table>
    <thead>
        <tr>
            <th>Folio del contrato</th>
            <th>Monto cobrado</th>
         
        </tr>
    </thead>
    <tbody>
        @foreach($contratos as $contrato)
        <tr>
            <td>{{ $contrato->folio }}</td>
            <td>{{ $contrato->montoCobrado }}</td>
        </tr>
        @endforeach
    </tbody>
</table>
<br>
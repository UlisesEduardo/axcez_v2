@extends('layouts.app')

@section('content')
<section class="content-header">
    <h1>
        Región
    </h1>
</section>
<div class="content">
    @include('adminlte-templates::common.errors')
    <div class="box box-primary">
        <div class="box-body">
            <div class="row">
                {!! Form::model($role, ['route' => ['roles.update', $role->id],
                'method' => 'PUT']) !!}

                @include('roles.form')

                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection
@extends('layouts.app')

@section('content')
<section class="content-header">
    <h1>
        Roles
    </h1>
</section>
<div class="content">
    <div class="box box-primary">
        <div class="box-body">
            <div class="row" style="padding-left: 20px">
                <p><strong>Nombre:</strong> {{ $role->name }}</p>
                <p><strong>Slug:</strong> {{ $role->slug }}</p>
                <p><strong>Descripción:</strong> {{ $role->description }}</p>
            </div>
        </div>
    </div>
</div>
@endsection
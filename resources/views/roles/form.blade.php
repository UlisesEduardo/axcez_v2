<div class="col-sm-6">
	{{ Form::label('name', 'Nombre de la etiqueta') }}
	{{ Form::text('name', null, ['class' => 'form-control', 'id' => 'name']) }}
</div>
<div class="col-sm-6">
	{{ Form::label('slug', 'URL Amigable') }}
	{{ Form::text('slug', null, ['class' => 'form-control', 'id' => 'slug']) }}
</div>
<div class="col-sm-12">
	{{ Form::label('description', 'Descripción') }}
	{{ Form::textarea('description', null, ['class' => 'form-control','rows' => '4']) }}
	<hr>
</div>


<div class="col-sm-12">
	<h3>Permiso especial</h3>
	<label>{{ Form::radio('special', 'all-access') }} Acceso total</label>
	<label>{{ Form::radio('special', 'no-access') }} Ningún acceso</label>
	<hr>
</div>

<div class="col-sm-12">
	<h3>Lista de permisos disponibles</h3>
</div>

<ul class="list-unstyled">
	@foreach($permissions as $permission)
	<li class="col-sm-4">
		<label>
			{{ Form::checkbox('permissions[]', $permission->id, null) }}
			{{ $permission->name }}
			<em>({{ $permission->description }})</em>
		</label>
	</li>
	@endforeach
</ul>
<br>
<div class="col-sm-12">
	{{ Form::submit('Guardar', ['class' => 'btn btn-sm btn-primary']) }}
</div>
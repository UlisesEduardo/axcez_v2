<div class="table-responsive">
    <table class="table" id="regions-table">
        <thead>
            <tr>
                <th>ID</th>
                <th>Nombre del rol</th>
                <th>Descripción</th>
            </tr>
        </thead>
        <tbody>

            @foreach($roles as $role)
            <tr>
                <td>{{ $role->id }}</td>
                <td>{{ $role->name }}</td>
                <td>{{ $role->description }}</td>
                <td>
                    {!! Form::open(['route' => ['roles.destroy', $role->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        @can('roles.show')
                        <a href="{{ route('roles.show', [$role->id]) }}" class='btn btn-default'><i class="icofont-eye"></i></a>
                        @endcan
                        @can('roles.edit')
                        <a href="{{ route('roles.edit', [$role->id]) }}" class='btn btn-default'><i class="icofont-ui-edit"></i></a>
                        @endcan
                        @can('roles.destroy')
                        {!! Form::button('<i class="icofont-ui-delete"></i>', ['type' => 'submit', 'class' => 'btn btn-danger', 'onclick' => "return confirm('Desea eliminar el registro?')"]) !!}
                        @endcan
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    {{$roles->links()}}
</div>
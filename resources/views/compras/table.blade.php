<div class="table-responsive">
    <table class="table" id="compras-table">
        <thead>
            <tr>
                <th>Fecha</th>
        <th>Total</th>
        <th>Proveedor</th>
        <th>Usuario</th>
            </tr>
        </thead>
        <tbody>
        @foreach($compras as $compra)
            <tr>
            <td>{{ $compra->fecha }}</td>
            <td>{{ $compra->total }}</td>
            <td>{{ $compra->nombre }}</td>
            <td>{{ $compra->usuario }}</td>
                <td>
                    <div class='btn-group'>
                        @can('compras.show')
                        <a href="{{ route('compras.show', [$compra->id]) }}" class='btn btn-default'><i class="icofont-eye"></i></a>
                        @endcan
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    {{$compras -> links()}}
</div>

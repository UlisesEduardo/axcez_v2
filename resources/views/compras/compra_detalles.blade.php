<!DOCTYPE html>
<html>

<head>
</head>

<body>
    <form id="formProductos" name="formProductos" method="post">

        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header">
                    <div class="form-group col-sm-3 pull-right">
                        <label for="fecha">Fecha de compra:</label>
                        <input type="date" name="fecha" class="form-control" id="fecha">
                    </div>
                    <div class="form-group col-sm-3 pull-right">
                        {!! Form::label('proveedor', 'Proveedor:') !!}
                        {!! Form::select('proveedor', $proveedores, null, ['class' => 'form-control']) !!}
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="form-group col-sm-3">
                        <label for="producto">Código de producto:</label>
                        <input type="text" name="codigoProducto" class="form-control" id="codigoProducto" style="text-transform: uppercase;">
                        <span id="errorCodigo" style="color: red"></span>
                    </div>
                    <div class="form-group col-sm-3">
                        <label for="cantidad">Cantidad comprada:</label>
                        <input type="number" name="cantidad" class="form-control" id="cantidad" onkeyup="calcularSubtotal()">
                        <span id="errorCantidad" style="color: red"></span>
                    </div>
                    <div class="form-group col-sm-3">
                        <label for="precio">Precio de compra:</label>
                        <input type="number" name="precio" class="form-control" id="precio" step="any" onkeyup="calcularSubtotal()">
                        <span id="errorPrecio" style="color: red"></span>
                    </div>
                    <div class="form-group col-sm-3">
                        <label for="subtotal">Subotal:</label>
                        <input type="text" name="subtotal" class="form-control" id="subtotal" readonly="readonly">
                    </div>

    </form>
    
    <div class="box-body col-sm-12">
        <div class="form-group col-sm-3">
            <input type="button" name="add" class="btn btn-primary" value="Agregar producto" id="add">
        </div>
        <div class="form-group col-sm-3 pull-right">
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalProductos">
                Mostrar productos registrados
            </button>
        </div>
    </div>

    <table id="tableProductos" name="tableProductos" class="table">
        <tbody>
            <tr>
                <th>No</th>
                <th>Código del producto</th>
                <th>Cantidad comprada</th>
                <th>Precio de compra</th>
                <th>Subtotal</th>
            <tr>
        </tbody>
    </table>
    <div class="form-group col-sm-3 pull-right">
        <label for="total">Total:</label>
        <input type="text" name="total" class="form-control" id="total" readonly="readonly">
    </div>
    <br>
    <div class="form-group col-sm-3">
        <button id="save" data-url="{{route('compras.store')}}" class="btn btn-primary">Guardar</button>
        <a href="{{ route('compras.index') }}" class="btn btn-default">Cancelar</a>
    </div>
    </div>
    <!-- /.box-body -->
    </div>
    <!-- /.box -->
    </div>

</body>

</html>


<!-- Modal -->
<div class="modal fade" id="modalProductos" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table class="table" id="productos-table">
                        <thead>
                            <tr>
                                <th>Código</th>
                                <th>Descripción</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($productos as $producto)
                            <tr>
                                <td>{{ $producto->codigo }}</td>
                                <td>{{ $producto->descipcion }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
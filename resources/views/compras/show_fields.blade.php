<!-- Fecha Field -->
<div class="form-group">
    {!! Form::label('fecha', 'Fecha de compra:') !!}
    <p>{{ $compras->fecha->format('Y-m-d') }}</p>
</div>

<!-- Total Field -->
<div class="form-group">
    {!! Form::label('total', 'Total:') !!}
    <p>{{ $compras->total }}</p>
</div>

<!-- Proveedor Field -->
<div class="form-group">
    {!! Form::label('proveedor', 'Proveedor:') !!}
    <p>{{ $proveedor[0]->nombre }}</p>
</div>

<!-- Usuario Field -->
<div class="form-group">
    {!! Form::label('usuario', 'Usuario que hizo el registro:') !!}
    <p>{{ $compras->usuario }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Registrado el:') !!}
    <p>{{ $compras->created_at }}</p>
</div>

<div class="table-responsive">
    <table class="table" id="comprasDetalles-table">
        <thead>
            <tr>
                <th>Código de producto</th>
                <th>Nombre y descripción</th>
                <th>Cantidad comprada</th>
                <th>Precio de compra</th>
                <th>Subtotal</th>
            </tr>
        </thead>
        <tbody>
            @foreach($comprasDetalles as $detalle)
            <tr>
                <td>{{ $detalle->codigo }}</td>
                <td>{{ $detalle->descipcion }}</td>
                <td>{{ $detalle->cantidad }}</td>
                <td>{{ $detalle->precioCompra }}</td>
                <td>{{ $detalle->subtotal }}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@extends('layouts.app')

@section('content')
<section class="content-header">
    <h1>
        Compras
    </h1>
</section>
<div class="content">
    @include('adminlte-templates::common.errors')

    <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
    {!! Form::open(['route' => 'compras.store']) !!}

    @include('compras.compra_detalles')

    {!! Form::close() !!}

</div>
@endsection
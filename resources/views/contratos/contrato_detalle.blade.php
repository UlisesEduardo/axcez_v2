<div class="container">
  <div class="panel-group" id="accordion">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">Datos del cliente</a>
        </h4>
      </div>
      <div class="panel-body">
        <div class="input-group input-group-sm col-sm-3 pull-right">
          <input type="text" id="codigoCliente" class="form-control" placeholder="Confirmar folio">
          <span class="input-group-btn">
            <button id="buscarCliente" href="#" data-toggle="modal" type="button" class="btn btn-success btn-flat"><i class="icofont-check-alt"></i></button>
          </span>
        </div>

        <div id="collapse1" class="panel-collapse collapse in">
          <div class="input-group input-group-sm col-sm-3 pull-right">
            <input type="text" id="nombresCliente" class="form-control" placeholder="Introduzca el nombre del cliente">
            <span class="input-group-btn">
              <button id="ver-clientes" href="#" data-toggle="modal" type="button" class="btn btn-info btn-flat"><i class="icofont-search-2"></i></button>
            </span>
          </div>
          <br>
          <div class="col-md">
            <div class="box box-solid">
              <div class="box-header with-border">
                <i class="icofont-user-alt-1"></i>

                <h3 class="box-title">Datos generales</h3>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                <!-- Codigo Field -->
                <div class="form-group col-sm-6">
                  {!! Form::label('codigo', 'Código del cliente:') !!}
                  {!! Form::text('codigo', null, ['class' => 'form-control','placeholder' => 'AXZ','readonly' => 'readonly']) !!}
                </div>

                <!-- Nombres Field -->
                <div class="form-group col-sm-6">
                  {!! Form::label('nombres', 'Nombre(s):') !!}
                  {!! Form::text('nombres', null, ['class' => 'form-control','required' => 'required']) !!}
                </div>

                <!-- Apellidopaterno Field -->
                <div class="form-group col-sm-6">
                  {!! Form::label('apellidoPaterno', 'Apellido paterno:') !!}
                  {!! Form::text('apellidoPaterno', null, ['class' => 'form-control','required' => 'required']) !!}
                </div>

                <!-- Apellidomaterno Field -->
                <div class="form-group col-sm-6">
                  {!! Form::label('apellidoMaterno', 'Apellido materno:') !!}
                  {!! Form::text('apellidoMaterno', null, ['class' => 'form-control','required' => 'required']) !!}
                </div>

              </div>
              <!-- /.box-body -->
            </div>
            <!-- /.box -->
          </div>

          <div class="col-md">
            <div class="box box-solid">
              <div class="box-header with-border">
                <i class="icofont-location-pin"></i>

                <h3 class="box-title">Datos del domicilio</h3>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                <!-- Municipio Field -->
                <div class="form-group col-sm-6">
                  {!! Form::label('municipio', 'Municipio:') !!}
                  {!! Form::text('municipio', null, ['class' => 'form-control','required' => 'required']) !!}
                </div>

                <!-- Barrio Field -->
                <div class="form-group col-sm-6">
                  {!! Form::label('barrio', 'Barrio:') !!}
                  {!! Form::text('barrio', null, ['class' => 'form-control','required' => 'required']) !!}
                </div>

                <!-- Calle Field -->
                <div class="form-group col-sm-6">
                  {!! Form::label('calle', 'Calle:') !!}
                  {!! Form::text('calle', null, ['class' => 'form-control','required' => 'required']) !!}
                </div>

                <!-- Num Field -->
                <div class="form-group col-sm-6">
                  {!! Form::label('num', 'No:') !!}
                  {!! Form::text('num', null, ['class' => 'form-control','required' => 'required']) !!}
                </div>

                <!-- Coordenadas Field -->
                <div class="form-group col-sm-6">
                  {!! Form::label('coordenadas', 'Coordenadas:') !!}
                  {!! Form::text('coordenadas', null, ['class' => 'form-control','required' => 'required']) !!}
                </div>

                <!-- Referencia Field -->
                <div class="form-group col-sm-12">
                  {!! Form::label('referencia', 'Referencia del domicilio:') !!}
                  {!! Form::textarea('referencia', null, ['class' => 'form-control','rows' => '4','required' => 'required']) !!}
                </div>
              </div>
              <!-- /.box-body -->
            </div>
            <!-- /.box -->
          </div>

          <div class="col-md">
            <div class="box box-solid">
              <div class="box-header with-border">
                <i class="icofont-ui-touch-phone"></i>

                <h3 class="box-title">Datos de contacto</h3>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                <!-- Telefono Field -->
                <div class="form-group col-sm-6">
                  {!! Form::label('telefono', 'Teléfono:') !!}
                  {!! Form::text('telefono', null, ['class' => 'form-control','required' => 'required']) !!}
                </div>

                <!-- Telefonoalternativo Field -->
                <div class="form-group col-sm-6">
                  {!! Form::label('telefonoAlternativo', 'Telefono alternativo:') !!}
                  {!! Form::text('telefonoAlternativo', null, ['class' => 'form-control','required' => 'required']) !!}
                </div>

                <!-- Correo Field -->
                <div class="form-group col-sm-6">
                  {!! Form::label('correo', 'Correo electrónico:') !!}
                  {!! Form::text('correo', null, ['class' => 'form-control','required' => 'required']) !!}
                </div>
              </div>
              <!-- /.box-body -->
            </div>
            <!-- /.box -->
          </div>
        </div>
      </div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">Datos del contrato</a>
        </h4>
      </div>
      <div id="collapse2" class="panel-collapse collapse">
        <div class="panel-body">
          <!-- Folio Field -->
          <div class="form-group col-sm-6">
            {!! Form::label('folio', 'Folio del contrato:') !!}
            {!! Form::text('folio', null, ['class' => 'form-control','placeholder' => 'FAP','readonly' => 'readonly']) !!}
          </div>

          <!-- Fechacontrato Field -->
          <div class="form-group col-sm-6">
            <label for="fechaContrato">Fecha de contratación:</label>
            <input type="date" name="fechaContrato" class="form-control" id="fechaContrato" required>
          </div>

          <!-- Fechainstalacion Field -->
          <div class="form-group col-sm-6">
            <label for="fechaInstalacion">Fecha de instalación:</label>
            <input type="date" name="fechaInstalacion" class="form-control" id="fechaInstalacion" required>
          </div>

          <!-- tuve que hacer el rellenado del select con foreach para agregar el 'seleccionar' -->
          <!-- y así obligar al usuario a seleccionar una opción, para que actue onchange de js -->
          <!-- Id Sector Field -->
          <div class="form-group col-sm-6">
            {!! Form::label('id_sector', 'Sectorial al que se conectará:') !!}
            <select id="id_sector" name="id_sector" class="form-control" required>
              <option>------Seleccionar------</option>
              @foreach( $sectorialItems as $key => $value )
              <option value="{{ $key }}">{{ $value }}</option>
              @endforeach
            </select>
          </div>
          <!-- Id Plan Field -->
          <div class="form-group col-sm-6">
            {!! Form::label('id_plan', 'Plan seleccionado:') !!}
            {!! Form::select('id_plan', $planItems, null, ['class' => 'form-control']) !!}
          </div>

          <!-- Ip Field -->
          <div class="form-group col-sm-6">
            {!! Form::label('ip', 'IP asignada:') !!}
            {!! Form::text('ip', null, ['class' => 'form-control'],'required') !!}
          </div>

          <!-- Gateway Field -->
          <div class="form-group col-sm-6">
            {!! Form::label('gateway', 'Gateway:') !!}
            {!! Form::text('gateway', null, ['class' => 'form-control','required']) !!}
          </div>

          <!-- Fechapago Field -->
          <div class="form-group col-sm-6">
            {!! Form::label('fechaPago', 'Fecha de pago:') !!}
            {!! Form::select('fechaPago', ['30' => '30', '15' => '15'], null, ['class' => 'form-control']) !!}
          </div>



          <!-- Tipocontrato Field -->
          <div class="form-group col-sm-6">
            {!! Form::label('tipoContrato', 'Tipo de contrato:') !!}
            {!! Form::select('tipoContrato', ['contado' => 'contado', 'diferido' => 'diferido'], null, ['class' => 'form-control']) !!}
          </div>
        </div>
      </div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">Material proporcionado</a>
        </h4>
      </div>
      <div id="collapse3" class="panel-collapse collapse">
        <div class="panel-body">
          <div class="form-group col-sm-3">
            <label for="producto">Código de producto:</label>
            <input type="text" name="codigoProducto" class="form-control" id="codigoProducto" style="text-transform: uppercase;">
          </div>
          <div class="form-group col-sm-3">
            <label for="cantidad">Cantidad vendida:</label>
            <input type="number" name="cantidad" class="form-control" id="cantidad" onkeyup="calcularSubtotal()">
          </div>
          <div class="form-group col-sm-3">
            <label for="precio">Precio de venta:</label>
            <input type="number" name="precio" class="form-control" id="precio" onkeyup="calcularSubtotal()">
            <span id="errorPrecio" style="color: red"></span>
          </div>
          <div class="form-group col-sm-3">
            <label for="subtotal">Subtotal:</label>
            <input type="text" name="subtotal" class="form-control" id="subtotal" readonly="readonly">
          </div>
          <div class="box-body col-sm-12">
            <div class="form-group col-sm-3">
              <input type="button" name="agregar" class="btn btn-primary" value="Agregar" id="agregar">
            </div>
            <div class="form-group col-sm-3 pull-right">
              <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#productos">
                Mostrar productos registrados
              </button>
            </div>
          </div>


          <table id="tableProductos" name="tableProductos" class="table table-bordered">
            <tbody>
              <tr>
                <th>No</th>
                <th>Código del producto</th>
                <th>Cantidad a vender</th>
                <th>Precio de venta</th>
                <th>Subtotal</th>
              <tr>
            </tbody>
          </table>
          <!-- Notasadicionales Field -->
          <div class="form-group col-lg-9">
            {!! Form::label('notasAdicionales', 'Notas adicionales:') !!}
            {!! Form::textarea('notasAdicionales', null, ['class' => 'form-control','rows' => '5','required']) !!}
          </div>

          <div class="form-group col-sm-3 pull-right">
            <!-- Montoreal Field -->
            {!! Form::label('montoReal', 'Monto real del contrato:') !!}
            {!! Form::number('montoReal', null, ['class' => 'form-control','required','step' => 'any','pattern' => '[0-9]+([\.,][0-9]+)?']) !!}
            <br>
            <!-- Montocobrado Field -->
            {!! Form::label('montoCobrado', 'Monto cobrado:') !!}
            {!! Form::number('montoCobrado', null, ['class' => 'form-control','required','step' => 'any','pattern' => '[0-9]+([\.,][0-9]+)?']) !!}
          </div>
          <!-- Submit Field -->
          <div class="form-group col-sm-12">
            <button id="guardarContrato" contrato-url="{{route('contratos.store')}}" class="btn btn-primary">Guardar</button>
            <a href="{{ route('contratos.index') }}" class="btn btn-default">Cancelar</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="productos" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-scrollable" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="table-responsive">
          <table class="table" id="productos-table">
            <thead>
              <tr>
                <th>Código</th>
                <th>Descripción</th>
                <th>Cantidad existente</th>
                <th>Precio de venta</th>
              </tr>
            </thead>
            <tbody>
              @foreach($productos as $producto)
              <tr>
                <td>{{ $producto->codigo }}</td>
                <td>{{ $producto->descipcion }}</td>
                <td>{{ $producto->cantidad }}</td>
                <td>{{ $producto->precioVenta }}</td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>


<!-- Modal -->
<div class="modal fade" id="modalClientes" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-scrollable" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="table-responsive">
          <table class="table">
            <thead>
              <th>Código</th>
              <th>Nombres</th>
              <th>Apellido paterno</th>
              <th>Apellido materno</th>
            </thead>
            <tbody id="tablaClientes">
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
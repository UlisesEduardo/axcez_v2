 <table class="table">
     <tr>
         <td>
             <!-- Folio Field -->
             {!! Form::label('folio', 'Folio del contrato:') !!}
             <p>{{ $contrato->folio }}</p>
         </td>
         <td>
             <div class="col-sm-3 pull-right">
                 <!-- Fechacontrato Field -->
                 {!! Form::label('fechaContrato', 'Fecha de contratación:') !!}
                 <p>{{ $contrato->fechaContrato->format('Y-m-d') }}</p>
             </div>
         </td>
     </tr>
 </table>

 <div class="box-header with-border">
     <i class="icofont-user-alt-1"></i>

     <h3 class="box-title">Cliente al que pertenece el contrato</h3>
 </div>
 <div class="box-body">
     <table class="table">
         <tr>
             <td>
                 <!-- Codigo Field -->
                 {!! Form::label('codigo', 'Código del cliente:') !!}
                 <p>{{ $cliente[0]->codigo }}</p>
             </td>

             <td>
                 <!-- Nombres Field -->
                 {!! Form::label('nombres', 'Nombre(s):') !!}
                 <p>{{ $cliente[0]->nombres }}</p>
             </td>

             <td>
                 <!-- Apellidopaterno Field -->
                 {!! Form::label('apellidoPaterno', 'Apellido paterno:') !!}
                 <p>{{ $cliente[0]->apellidoPaterno }}</p>
             </td>

             <td>
                 <!-- Apellidomaterno Field -->
                 {!! Form::label('apellidoMaterno', 'Apellido materno:') !!}
                 <p>{{ $cliente[0]->apellidoMaterno }}</p>
             </td>
         </tr>
     </table>
 </div>


 <div class="box-header with-border">
     <i class="icofont-paperclip"></i>

     <h3 class="box-title">Datos del contrato</h3>
 </div>
 <div class="box-body">
     <table class="table">
         <tr>
             <td>
                 <!-- Fechainstalacion Field -->
                 {!! Form::label('fechaInstalacion', 'Fecha de instalación:') !!}
                 <p>{{ $contrato->fechaInstalacion->format('Y-m-d') }}</p>
             </td>
             <td>
                 <!-- Id Sector Field -->
                 {!! Form::label('id_sector', 'Sectorial al que se conectó:') !!}
                 <p>{{ $contrato->sector->nombre }}</p>
             </td>
             <td>
                 <!-- Ip Field -->
                 {!! Form::label('ip', 'IP:') !!}
                 <p>{{ $contrato->ip }}</p>
             </td>
             <td>
                 <!-- Gateway Field -->
                 {!! Form::label('gateway', 'Gateway:') !!}
                 <p>{{ $contrato->gateway }}</p>
             </td>
         </tr>
         <tr>
             <td>
                 <!-- Id Plan Field -->
                 {!! Form::label('id_plan', 'Plan seleccionado:') !!}
                 <p>{{ $contrato->plan->nombre }}</p>
             </td>
             <td>
                 <!-- Fechapago Field -->
                 {!! Form::label('fechaPago', 'Fecha de pago:') !!}
                 <p>{{ $contrato->fechaPago }}</p>
             </td>
             <td>
                 <!-- Tipocontrato Field -->
                 {!! Form::label('tipoContrato', 'Tipo contrato:') !!}
                 <p>{{ $contrato->tipoContrato }}</p>
             </td>
         </tr>
     </table>
 </div>

 <div class="box-header with-border">
     <i class="icofont-ui-wifi"></i>

     <h3 class="box-title">Material proporcionado</h3>
 </div>
 <div class="box-body">
     <div class="table-responsive">
         <table class="table" id="productos-table">
             <thead>
                 <tr>
                     <th>Código del producto</th>
                     <th>Nombre y descripción</th>
                     <th>Cantidad vendida</th>
                     <th>Precio de venta</th>
                     <th>subtotal</th>
                 </tr>
             </thead>
             <tbody>
                 @foreach($productos as $producto)
                 <tr>
                     <td>{{ $producto->codigo }}</td>
                     <td>{{ $producto->descipcion }}</td>
                     <td>{{ $producto->cantidad }}</td>
                     <td>{{ $producto->precioVenta }}</td>
                     <td>{{ $producto->subtotal }}</td>
                 </tr>
                 @endforeach
             </tbody>
         </table>
     </div>
 </div>

 <table class="table">
     <tr>
         <td>
             <!-- Notasadicionales Field -->
             {!! Form::label('notasAdicionales', 'Notas adicionales:') !!}
             <p>{{ $contrato->notasAdicionales }}</p>
         </td>
         <td>
             <div class="col-sm-3 pull-right">
                 <!-- Montoreal Field -->
                 {!! Form::label('montoReal', 'Monto real del contrato:') !!}
                 <p>{{ $contrato->montoReal }}</p>

                 <!-- Montocobrado Field -->
                 {!! Form::label('montoCobrado', 'Monto cobrado:') !!}
                 <p>{{ $contrato->montoCobrado }}</p>
             </div>
         </td>
     </tr>
 </table>
 <br>
 <table class="table">
     <tr>
         <td>
             <div class="col-sm-6">
                 <strong>Nombre y firma del proveedor</strong>
             </div>

         </td>
         <td>
             <div class="col-sm-6 pull-right">
                 <strong>Nombre y firma del cliente</strong>
             </div>
         </td>
     </tr>
 </table>

@section('contentPanel')
<div class="table-responsive">
    <table class="table" id="contratos-table">
        <thead>
            <tr>
                <th>Folio</th>
                <th>Contratado el</th>
                <th>Instalación</th>
                <th>Plan</th>
                <th>IP</th>
                <th>Gateway</th>
                <th>Fecha de pagos</th>
                <th>Monto cobrado</th>
                <th>Tipo</th>
            </tr>
        </thead>
        <tbody>
            @foreach($contratos as $contrato)
            <tr>
                <td>{{ $contrato->folio }}</td>
                <td>{{ $contrato->fechaContrato }}</td>
                <td>{{ $contrato->fechaInstalacion }}</td>
                <td>{{ $contrato->nombre }}</td>
                <td>{{ $contrato->ip }}</td>
                <td>{{ $contrato->gateway }}</td>
                <td>{{ $contrato->fechaPago }}</td>
                <td>{{ $contrato->montoCobrado }}</td>
                <td>{{ $contrato->tipoContrato }}</td>
                <td>
                    {!! Form::open(['route' => ['contratos.destroy', $contrato->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        @can('contratos.show')
                        <a href="{{ route('contratos.show', [$contrato->id]) }}" class='btn btn-default'><i class="icofont-eye"></i></a>
                        @endcan
                        @can('contratos.edit')
                        <a href="{{ route('contratos.edit', [$contrato->id]) }}" class='btn btn-default'><i class="icofont-ui-edit"></i></a>
                        @endcan
                        @can('contratos.destroy')
                        {!! Form::button('<i class="icofont-ui-delete"></i>', ['type' => 'submit', 'class' => 'btn btn-danger', 'onclick' => "return confirm('Desea eliminar el registro?')"]) !!}
                        @endcan
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection
@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Contrato
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
                    {!! Form::open(['route' => 'contratos.store']) !!}

                        @include('contratos.contrato_detalle')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection

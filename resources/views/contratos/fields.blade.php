<div class="box-header with-border">
    <i class="icofont-user-alt-1"></i>

    <h3 class="box-title">Cliente al que pertenece el contrato</h3>
</div>
<div class="box-body">
    <!-- Codigo Field -->
    <div class="form-group">
        {!! Form::label('codigo', 'Código del cliente:') !!}
        <p>{{ $cliente[0]->codigo }}</p>
    </div>

    <!-- Nombres Field -->
    <div class="form-group">
        {!! Form::label('nombres', 'Nombre(s):') !!}
        <p>{{ $cliente[0]->nombres }}</p>
    </div>

    <!-- Apellidopaterno Field -->
    <div class="form-group">
        {!! Form::label('apellidoPaterno', 'Apellido paterno:') !!}
        <p>{{ $cliente[0]->apellidoPaterno }}</p>
    </div>

    <!-- Apellidomaterno Field -->
    <div class="form-group">
        {!! Form::label('apellidoMaterno', 'Apellido materno:') !!}
        <p>{{ $cliente[0]->apellidoMaterno }}</p>
    </div>
</div>

<div class="box-header with-border">
    <i class="icofont-paperclip"></i>

    <h3 class="box-title">Datos del contrato</h3>
</div>
<div class="box-body">
    <!-- Folio Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('folio', 'Folio:') !!}
        {!! Form::text('folio', null, ['class' => 'form-control','readonly' => 'readonly']) !!}
    </div>

    <!-- Fechacontrato Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('fechaContrato', 'Fechacontrato:') !!}
        @if(empty($contrato))
        <!-- verificamos si nos pasan algo, si esta vacío el control no carga nada -->
        {!! Form::date('fechaContrato', null, ['class' => 'form-control','id'=>'fechaContrato']) !!}
        @else
        <!-- De lo contrario nos cargan los datos -->
        {!! Form::date('fechaContrato', $contrato->fechaContrato, ['class' => 'form-control','id'=>'fechaContrato','readonly' => 'readonly']) !!}
        @endif

    </div>

    <!-- Fechainstalacion Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('fechaInstalacion', 'Fechainstalacion:') !!}
        @if(empty($contrato))
        <!-- verificamos si nos pasan algo, si esta vacío el control no carga nada -->
        {!! Form::date('fechaInstalacion', null, ['class' => 'form-control','id'=>'fechaInstalacion']) !!}
        @else
        <!-- De lo contrario nos cargan los datos -->
        {!! Form::date('fechaInstalacion', $contrato->fechaInstalacion, ['class' => 'form-control','id'=>'fechaInstalacion','readonly' => 'readonly']) !!}
        @endif

    </div>

    <!-- Id Sector Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('id_sector', 'Id Sector:') !!}
        {!! Form::select('id_sector', $sectorialItems, null, ['class' => 'form-control']) !!}
    </div>

    <!-- Id Plan Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('id_plan', 'Id Plan:') !!}
        {!! Form::select('id_plan', $planItems, null, ['class' => 'form-control']) !!}
    </div>

    <!-- Ip Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('ip', 'Ip:') !!}
        {!! Form::text('ip', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Gateway Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('gateway', 'Gateway:') !!}
        {!! Form::text('gateway', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Fechapago Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('fechaPago', 'Fechapago:') !!}
        {!! Form::select('fechaPago', ['30' => '30', '15' => '15'], null, ['class' => 'form-control']) !!}
    </div>

    <!-- Montoreal Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('montoReal', 'Montoreal:') !!}
        {!! Form::number('montoReal', null, ['class' => 'form-control','readonly' => 'readonly']) !!}
    </div>

    <!-- Montocobrado Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('montoCobrado', 'Montocobrado:') !!}
        {!! Form::number('montoCobrado', null, ['class' => 'form-control','readonly' => 'readonly']) !!}
    </div>

    <!-- Tipocontrato Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('tipoContrato', 'Tipocontrato:') !!}
        {!! Form::select('tipoContrato', ['contado' => 'contado', 'diferido' => 'diferido'], null, ['class' => 'form-control','readonly' => 'readonly']) !!}
    </div>

    <!-- Notasadicionales Field -->
    <div class="form-group col-lg-12">
        {!! Form::label('notasAdicionales', 'Notasadicionales:') !!}
        {!! Form::textarea('notasAdicionales', null, ['class' => 'form-control','rows' => '4']) !!}
    </div>
</div>



<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('contratos.index') }}" class="btn btn-default">Cancelar</a>
</div>
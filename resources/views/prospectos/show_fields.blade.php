<div class="box-header with-border">
    <i class="icofont-user-alt-1"></i>

    <h3 class="box-title">Datos generales</h3>
</div>
<div class="box-body">
    <!-- Nombres Field -->
    <div class="form-group">
        {!! Form::label('nombres', 'Nombre(s):') !!}
        <p>{{ $prospecto->nombres }}</p>
    </div>

    <!-- Apellidopaterno Field -->
    <div class="form-group">
        {!! Form::label('apellidoPaterno', 'Apellido paterno:') !!}
        <p>{{ $prospecto->apellidoPaterno }}</p>
    </div>

    <!-- Apellidomaterno Field -->
    <div class="form-group">
        {!! Form::label('apellidoMaterno', 'Apellido materno:') !!}
        <p>{{ $prospecto->apellidoMaterno }}</p>
    </div>
</div>


<div class="box-header with-border">
    <i class="icofont-location-pin"></i>

    <h3 class="box-title">Datos del domicilio</h3>
</div>
<div class="box-body">
    <!-- Municipio Field -->
    <div class="form-group">
        {!! Form::label('municipio', 'Municipio:') !!}
        <p>{{ $prospecto->municipio }}</p>
    </div>

    <!-- Barrio Field -->
    <div class="form-group">
        {!! Form::label('barrio', 'Barrio:') !!}
        <p>{{ $prospecto->barrio }}</p>
    </div>

    <!-- Calle Field -->
    <div class="form-group">
        {!! Form::label('calle', 'Calle:') !!}
        <p>{{ $prospecto->calle }}</p>
    </div>

    <!-- Num Field -->
    <div class="form-group">
        {!! Form::label('num', 'No:') !!}
        <p>{{ $prospecto->num }}</p>
    </div>

    <!-- Referencia Field -->
    <div class="form-group">
        {!! Form::label('referencia', 'Referencia del domicilio:') !!}
        <p>{{ $prospecto->referencia }}</p>
    </div>

    <!-- Coordenadas Field -->
    <div class="form-group">
        {!! Form::label('coordenadas', 'Coordenadas:') !!}
        <p>{{ $prospecto->coordenadas }}</p>
    </div>
</div>


<div class="box-header with-border">
    <i class="icofont-ui-touch-phone"></i>

    <h3 class="box-title">Datos de contacto</h3>
</div>

<div class="box-body">
    <!-- Telefono Field -->
    <div class="form-group">
        {!! Form::label('telefono', 'Teléfono:') !!}
        <p>{{ $prospecto->telefono }}</p>
    </div>

    <!-- Telefonoalternativo Field -->
    <div class="form-group">
        {!! Form::label('telefonoAlternativo', 'Teléfono alternativo:') !!}
        <p>{{ $prospecto->telefonoAlternativo }}</p>
    </div>

    <!-- Correo Field -->
    <div class="form-group">
        {!! Form::label('correo', 'Correo electrónico:') !!}
        <p>{{ $prospecto->correo }}</p>
    </div>

    <!-- Notas Field -->
    <div class="form-group">
        {!! Form::label('notas', 'Notas:') !!}
        <p>{{ $prospecto->notas }}</p>
    </div>

    <!-- Created At Field -->
    <div class="form-group">
        {!! Form::label('created_at', 'Registrado el:') !!}
        <p>{{ $prospecto->created_at }}</p>
    </div>
</div>
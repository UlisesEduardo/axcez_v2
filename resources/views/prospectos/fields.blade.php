 <div class="box-header with-border">
     <i class="icofont-user-alt-1"></i>

     <h3 class="box-title">Datos generales</h3>
 </div>
 <div class="box-body">
     <!-- Nombres Field -->
     <div class="form-group col-sm-6">
         {!! Form::label('nombres', 'Nombre(s):') !!}
         {!! Form::text('nombres', null, ['class' => 'form-control']) !!}
     </div>

     <!-- Apellidopaterno Field -->
     <div class="form-group col-sm-6">
         {!! Form::label('apellidoPaterno', 'Apellido paterno:') !!}
         {!! Form::text('apellidoPaterno', null, ['class' => 'form-control']) !!}
     </div>

     <!-- Apellidomaterno Field -->
     <div class="form-group col-sm-6">
         {!! Form::label('apellidoMaterno', 'Apellido materno:') !!}
         {!! Form::text('apellidoMaterno', null, ['class' => 'form-control']) !!}
     </div>
 </div>





 <div class="box-header with-border">
     <i class="icofont-location-pin"></i>

     <h3 class="box-title">Datos del domicilio</h3>
 </div>
 <div class="box-body">
     <!-- Municipio Field -->
     <div class="form-group col-sm-6">
         {!! Form::label('municipio', 'Municipio:') !!}
         {!! Form::text('municipio', null, ['class' => 'form-control']) !!}
     </div>

     <!-- Barrio Field -->
     <div class="form-group col-sm-6">
         {!! Form::label('barrio', 'Barrio:') !!}
         {!! Form::text('barrio', null, ['class' => 'form-control']) !!}
     </div>

     <!-- Calle Field -->
     <div class="form-group col-sm-6">
         {!! Form::label('calle', 'Calle:') !!}
         {!! Form::text('calle', null, ['class' => 'form-control']) !!}
     </div>

     <!-- Num Field -->
     <div class="form-group col-sm-6">
         {!! Form::label('num', 'No:') !!}
         {!! Form::text('num', null, ['class' => 'form-control']) !!}
     </div>

     <!-- Referencia Field -->
     <div class="form-group col-lg-12">
         {!! Form::label('referencia', 'Referencia del domicilio:') !!}
         {!! Form::textarea('referencia', null, ['class' => 'form-control','rows' => '4']) !!}
     </div>

     <!-- Coordenadas Field -->
     <div class="form-group col-sm-6">
         {!! Form::label('coordenadas', 'Coordenadas:') !!}
         {!! Form::text('coordenadas', null, ['class' => 'form-control']) !!}
     </div>
 </div>



 <div class="box-header with-border">
     <i class="icofont-ui-touch-phone"></i>

     <h3 class="box-title">Datos de contacto</h3>
 </div>
 <!-- /.box-header -->
 <div class="box-body">
     <!-- Telefono Field -->
     <div class="form-group col-sm-6">
         {!! Form::label('telefono', 'Teléfono:') !!}
         {!! Form::text('telefono', null, ['class' => 'form-control']) !!}
     </div>

     <!-- Telefonoalternativo Field -->
     <div class="form-group col-sm-6">
         {!! Form::label('telefonoAlternativo', 'Teléfono alternativo:') !!}
         {!! Form::text('telefonoAlternativo', null, ['class' => 'form-control']) !!}
     </div>

     <!-- Correo Field -->
     <div class="form-group col-sm-6">
         {!! Form::label('correo', 'Correo electrónico:') !!}
         {!! Form::text('correo', null, ['class' => 'form-control']) !!}
     </div>
     <!-- Notas Field -->
     <div class="form-group col-lg-12">
         {!! Form::label('notas', 'Notas:') !!}
         {!! Form::textarea('notas', null, ['class' => 'form-control','rows' => '4']) !!}
     </div>
 </div>

 <!-- Submit Field -->
 <div class="form-group col-sm-12">
     {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
     <a href="{{ route('prospectos.index') }}" class="btn btn-default">Cancelar</a>
 </div>
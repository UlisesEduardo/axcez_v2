<div class="table-responsive">
    <table class="table" id="prospectos-table">
        <thead>
            <tr>
                <th>Nombre completo</th>
                <th>Municipio</th>
                <th>Barrio</th>
                <th>Calle</th>
                <th>No</th>
                <th>Referencia</th>
                <th>Teléfono</th>
                <th>Correo</th>
            </tr>
        </thead>
        <tbody>
            @foreach($prospectos as $prospecto)
            <tr>
                <td>{{ $prospecto->nombres }} {{ $prospecto->apellidoPaterno }} {{ $prospecto->apellidoMaterno }}</td>
                <td>{{ $prospecto->municipio }}</td>
                <td>{{ $prospecto->barrio }}</td>
                <td>{{ $prospecto->calle }}</td>
                <td>{{ $prospecto->num }}</td>
                <td>{{ $prospecto->referencia }}</td>
                <td>{{ $prospecto->telefono }}</td>
                <td>{{ $prospecto->correo }}</td>
                <td>
                    {!! Form::open(['route' => ['prospectos.destroy', $prospecto->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        @can('prospectos.show')
                        <a href="{{ route('prospectos.show', [$prospecto->id]) }}" class='btn btn-default btn-xs'><i class="icofont-eye"></i></a>
                        @endcan
                        @can('prospectos.edit')
                        <a href="{{ route('prospectos.edit', [$prospecto->id]) }}" class='btn btn-default btn-xs'><i class="icofont-ui-edit"></i></a>
                        @endcan
                        @can('prospectos.destroy')
                        {!! Form::button('<i class="icofont-ui-delete"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Desea eliminar el registro?')"]) !!}
                        @endcan
                        {!! Form::button('<i class="icofont-brand-whatsapp"></i>', [ 'class' => 'btn btn-outline-success btn-xs', 'onclick' => " window.open('https://api.whatsapp.com/send?phone=52'+$prospecto->telefono);"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    {{$prospectos -> links()}}
</div>
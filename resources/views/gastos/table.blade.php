<div class="table-responsive">
    <table class="table" id="gastos-table">
        <thead>
            <tr>
                <th>Concepto</th>
                <th>Total</th>
                <th>Fecha</th>
                <th>Usuario que hizo el registro</th>
            </tr>
        </thead>
        <tbody>
            @foreach($gastos as $gasto)
            <tr>
                <td>{{ $gasto->concepto }}</td>
                <td>{{ $gasto->total }}</td>
                <td>{{ $gasto->fecha->format('Y-m-d') }}</td>
                <td>{{ $gasto->usuario }}</td>
                <td>
                    {!! Form::open(['route' => ['gastos.destroy', $gasto->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        @can('gastos.show')
                        <a href="{{ route('gastos.show', [$gasto->id]) }}" class='btn btn-default'><i class="icofont-eye"></i></a>
                        @endcan
                        @can('gastos.edit')
                        <a href="{{ route('gastos.edit', [$gasto->id]) }}" class='btn btn-default'><i class="icofont-ui-edit"></i></a>
                        @endcan
                        @can('gastos.destroy')
                        {!! Form::button('<i class="icofont-ui-delete"></i>', ['type' => 'submit', 'class' => 'btn btn-danger', 'onclick' => "return confirm(Desea eliminar el registro?')"]) !!}
                        @endcan
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    {{$gastos -> links()}}
</div>
<!-- Concepto Field -->
<div class="form-group col-sm-6">
    {!! Form::label('concepto', 'Concepto:') !!}
    {!! Form::text('concepto', null, ['class' => 'form-control']) !!}
</div>

<!-- Total Field -->
<div class="form-group col-sm-6">
    {!! Form::label('total', 'Total:') !!}
    {!! Form::number('total', null, ['class' => 'form-control','step' => 'any']) !!}
</div>

<!-- Fecha Field -->
<!-- En esta parte, el valor de fecha no cargaba en el update por el formato que estaba en YYYT-mm-dd HH:mm:ss -->
<div class="form-group col-sm-6">
    {!! Form::label('fecha', 'Fecha:') !!}
    @if(empty($gastos))
    <!-- verificamos si nos pasan algo, si esta vacío el control no carga nada -->
    {!! Form::date('fecha', null, ['class' => 'form-control','id'=>'fecha']) !!}
    @else
    <!-- De lo contrario nos cargan los datos -->
    {!! Form::date('fecha', $gastos->fecha, ['class' => 'form-control','id'=>'fecha']) !!}
    @endif
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('gastos.index') }}" class="btn btn-default">Cancelar</a>
</div>
<!-- Concepto Field -->
<div class="form-group">
    {!! Form::label('concepto', 'Concepto:') !!}
    <p>{{ $gastos->concepto }}</p>
</div>

<!-- Total Field -->
<div class="form-group">
    {!! Form::label('total', 'Total:') !!}
    <p>{{ $gastos->total }}</p>
</div>

<!-- Fecha Field -->
<div class="form-group">
    {!! Form::label('fecha', 'Fecha:') !!}
    <p>{{ $gastos->fecha->format('Y-m-d') }}</p>
</div>

<!-- Usuario Field -->
<div class="form-group">
    {!! Form::label('usuario', 'Usuario que hizo el registro:') !!}
    <p>{{ $gastos->usuario }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Registrado el:') !!}
    <p>{{ $gastos->created_at }}</p>
</div>



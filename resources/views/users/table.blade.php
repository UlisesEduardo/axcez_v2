<div class="table-responsive">
    <table class="table" id="sectorials-table">
        <thead>
            <tr>
                <th>ID</th>
                <th>Nombre de usuario</th>
                <th>Correo electrónico</th>
            </tr>
        </thead>
        <tbody>
            @foreach($users as $user)
            <tr>
                <td>{{ $user->id }}</td>
                <td>{{ $user->name }}</td>
                <td>{{ $user->email }}</td>
                <td>
                    {!! Form::open(['route' => ['users.destroy', $user->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        @can('users.show')
                        <a href="{{ route('users.show', [$user->id]) }}" class='btn btn-default'><i class="icofont-eye"></i></a>
                        @endcan
                        @can('users.edit')
                        <a href="{{ route('users.edit', [$user->id]) }}" class='btn btn-default'><i class="icofont-ui-edit"></i></a>
                        @endcan
                        @can('users.destroy')
                        {!! Form::button('<i class="icofont-ui-delete"></i>', ['type' => 'submit', 'class' => 'btn btn-danger', 'onclick' => "return confirm('Desea eliminar el registro?')"]) !!}
                        @endcan
                        {!! Form::close() !!}
                    </div>

                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    {{$users -> links()}}
</div>
<div class="col-sm-6">
	{{ Form::label('name', 'Nombre de usuario') }}
	{{ Form::text('name', null, ['class' => 'form-control', 'id' => 'name']) }}
</div>
<br>

<div class="col-sm-12">
	<hr>
	<h4>Lista de roles</h4>
	<ul class="list-unstyled">
		@foreach($roles as $role)
		<li>
			<label>
				{{ Form::checkbox('roles[]', $role->id, null) }}
				{{ $role->name }}
				<em>({{ $role->description }})</em>
			</label>
		</li>
		@endforeach
	</ul>
</div>
<div class="col-sm-6">
	{{ Form::submit('Guardar', ['class' => 'btn btn-sm btn-primary']) }}
</div>
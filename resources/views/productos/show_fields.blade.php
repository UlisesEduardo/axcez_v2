<!-- Codigo Field -->
<div class="form-group">
    {!! Form::label('codigo', 'Código del producto:') !!}
    <p>{{ $producto->codigo }}</p>
</div>

<!-- Descipcion Field -->
<div class="form-group">
    {!! Form::label('descipcion', 'Nombre y descripción:') !!}
    <p>{{ $producto->descipcion }}</p>
</div>

<!-- Cantidad Field -->
<div class="form-group">
    {!! Form::label('cantidad', 'Cantidad existente:') !!}
    <p>{{ $producto->cantidad }}</p>
</div>

<!-- Precioventa Field -->
<div class="form-group">
    {!! Form::label('precioVenta', 'Precio de venta:') !!}
    <p>{{ $producto->precioVenta }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Registrado el:') !!}
    <p>{{ $producto->created_at }}</p>
</div>



<div class="table-responsive">
    <table class="table" id="productos-table">
        <thead>
            <tr>
                <th>Código del producto</th>
                <th>Nombre y descripción</th>
                <th>Cantidad existente</th>
                <th>Precio de venta</th>
            </tr>
        </thead>
        <tbody>
            @foreach($productos as $producto)
            <tr>
                <td>{{ $producto->codigo }}</td>
                <td>{{ $producto->descipcion }}</td>
                <td>{{ $producto->cantidad }}</td>
                <td>{{ $producto->precioVenta }}</td>
                <td>
                    {!! Form::open(['route' => ['productos.destroy', $producto->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        @can('productos.show')
                        <a href="{{ route('productos.show', [$producto->id]) }}" class='btn btn-default'><i class="icofont-eye"></i></a>
                        @endcan
                        @can('productos.edit')
                        <a href="{{ route('productos.edit', [$producto->id]) }}" class='btn btn-default'><i class="icofont-ui-edit"></i></a>
                        @endcan
                        @can('productos.destroy')
                        {!! Form::button('<i class="icofont-ui-delete"></i>', ['type' => 'submit', 'class' => 'btn btn-danger', 'onclick' => "return confirm('Desea eliminar el registro?')"]) !!}
                        @endcan
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    {{$productos -> links()}}
</div>
@extends('layouts.app')

@section('content')
<section class="content-header">
    <h1>
        Producto
    </h1>
</section>
<div class="content">
    @include('adminlte-templates::common.errors')
    <div class="box box-primary">
        <div class="box-body">
            <div class="form-group col-sm-12">
                <div class="alert alert-success alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-info"></i> Importante!</h4>
                    Dejar los campos cantidad y precio de venta en 0, ya que esa información será determinada al hacer una compra.
                </div>
            </div>
            <div class="row">
                {!! Form::open(['route' => 'productos.store']) !!}

                @include('productos.fields')

                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection
<!-- Codigo Field -->
<div class="form-group col-sm-6">
    {!! Form::label('codigo', 'Código del producto:') !!}
    {!! Form::text('codigo', null, ['class' => 'form-control','style' => 'text-transform: uppercase;']) !!}
</div>

<!-- Descipcion Field -->
<div class="form-group col-sm-6">
    {!! Form::label('descipcion', 'Nombre y descripción:') !!}
    {!! Form::text('descipcion', null, ['class' => 'form-control']) !!}
</div>

<!-- Cantidad Field -->
<div class="form-group col-sm-6">
    {!! Form::label('cantidad', 'Cantidad:') !!}
    {!! Form::number('cantidad', null, ['class' => 'form-control','placeholder' => 'Dejar en 0']) !!}
</div>

<!-- Precioventa Field -->
<div class="form-group col-sm-6">
    {!! Form::label('precioVenta', 'Precio de venta:') !!}
    {!! Form::number('precioVenta', null, ['class' => 'form-control','placeholder' => 'Dejar en 0','step' => 'any']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('productos.index') }}" class="btn btn-default">Cancelar</a>
</div>
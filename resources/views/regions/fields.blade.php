<!-- Nombreregion Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nombreRegion', 'Nombre de la región:') !!}
    {!! Form::text('nombreRegion', null, ['class' => 'form-control','maxlength' => 50]) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('regions.index') }}" class="btn btn-default">Cancelar</a>
</div>

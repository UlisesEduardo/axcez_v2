<!-- Nombreregion Field -->
<div class="form-group">
    {!! Form::label('nombreRegion', 'Nombre de la región:') !!}
    <p>{{ $region->nombreRegion }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Registrada el:') !!}
    <p>{{ $region->created_at }}</p>
</div>

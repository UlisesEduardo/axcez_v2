<div class="table-responsive">
    <table class="table" id="regions-table">
        <thead>
            <tr>
                <th>Nombre de la región</th>
            </tr>
        </thead>
        <tbody>

            @foreach($regions as $region)
            <tr>
                <td>{{ $region->nombreRegion }}</td>
                <td>
                    {!! Form::open(['route' => ['regions.destroy', $region->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        @can('regions.show')
                        <a href="{{ route('regions.show', [$region->id]) }}" class='btn btn-default'><i class="icofont-eye"></i></a>
                        @endcan
                        @can('regions.edit')
                        <a href="{{ route('regions.edit', [$region->id]) }}" class='btn btn-default'><i class="icofont-ui-edit"></i></a>
                        @endcan
                        @can('regions.destroy')
                        {!! Form::button('<i class="icofont-ui-delete"></i>', ['type' => 'submit', 'class' => 'btn btn-danger', 'onclick' => "return confirm('Desea eliminar el registro?')"]) !!}
                        @endcan
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    {{$regions->links()}}
</div>
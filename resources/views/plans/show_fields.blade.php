<!-- Id Region Field -->
<div class="form-group">
    {!! Form::label('id_region', 'Región:') !!}
    <p>{{ $plan->region->nombreRegion }}</p>
</div>

<!-- Nombre Field -->
<div class="form-group">
    {!! Form::label('nombre', 'Nombre:') !!}
    <p>{{ $plan->nombre }}</p>
</div>

<!-- Subida Field -->
<div class="form-group">
    {!! Form::label('subida', 'Velocidad de subida:') !!}
    <p>{{ $plan->subida }}</p>
</div>

<!-- Bajada Field -->
<div class="form-group">
    {!! Form::label('bajada', 'Velocidad de bajada:') !!}
    <p>{{ $plan->bajada }}</p>
</div>

<!-- Precio Field -->
<div class="form-group">
    {!! Form::label('precio', 'Precio ofertado al público:') !!}
    <p>{{ $plan->precio }}</p>
</div>

<!-- Preciomin Field -->
<div class="form-group">
    {!! Form::label('precioMin', 'Precio mínimo:') !!}
    <p>{{ $plan->precioMin }}</p>
</div>

<!-- Preciomax Field -->
<div class="form-group">
    {!! Form::label('precioMax', 'Precio máximo:') !!}
    <p>{{ $plan->precioMax }}</p>
</div>

<!-- Descripcion Field -->
<div class="form-group">
    {!! Form::label('descripcion', 'Descripción:') !!}
    <p>{{ $plan->descripcion }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Registrado el:') !!}
    <p>{{ $plan->created_at }}</p>
</div>

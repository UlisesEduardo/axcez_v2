<div class="table-responsive">
    <table class="table" id="plans-table">
        <thead>
            <tr>
                <th>Región</th>
                <th>Nombre</th>
                <th>Velocidad de subida</th>
                <th>Velocidad de bajada</th>
                <th>Precio ofertado</th>
                <th>Precio mínimo</th>
                <th>Precio máximo</th>
                <th>Descripción</th>
            </tr>
        </thead>
        <tbody>
            @foreach($plans as $plan)
            <tr>
                <td>{{ $plan->region->nombreRegion }}</td>
                <td>{{ $plan->nombre }}</td>
                <td>{{ $plan->subida }}</td>
                <td>{{ $plan->bajada }}</td>
                <td>{{ $plan->precio }}</td>
                <td>{{ $plan->precioMin }}</td>
                <td>{{ $plan->precioMax }}</td>
                <td>{{ $plan->descripcion }}</td>
                <td>
                    {!! Form::open(['route' => ['plans.destroy', $plan->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        @can('plans.show')
                        <a href="{{ route('plans.show', [$plan->id]) }}" class='btn btn-default'><i class="icofont-eye"></i></a>
                        @endcan
                        @can('plans.edit')
                        <a href="{{ route('plans.edit', [$plan->id]) }}" class='btn btn-default'><i class="icofont-ui-edit"></i></a>
                        @endcan
                        @can('plans.destroy')
                        {!! Form::button('<i class="icofont-ui-delete"></i>', ['type' => 'submit', 'class' => 'btn btn-danger', 'onclick' => "return confirm('Desea eliminar el registro?')"]) !!}
                        @endcan
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    {{$plans -> links()}}
</div>
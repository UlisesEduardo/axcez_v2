<!-- Id Region Field -->
<div class="form-group col-sm-6">
    {!! Form::label('id_region', 'Región:') !!}
    {!! Form::select('id_region', $regionItems, null, ['class' => 'form-control']) !!}
</div>

<!-- Nombre Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nombre', 'Nombre:') !!}
    {!! Form::text('nombre', null, ['class' => 'form-control']) !!}
</div>

<!-- Subida Field -->
<div class="form-group col-sm-6">
    {!! Form::label('subida', 'Velocidad de subida:') !!}
    {!! Form::text('subida', null, ['class' => 'form-control']) !!}
</div>

<!-- Bajada Field -->
<div class="form-group col-sm-6">
    {!! Form::label('bajada', 'Velocidad de bajada:') !!}
    {!! Form::text('bajada', null, ['class' => 'form-control']) !!}
</div>

<!-- Precio Field -->
<div class="form-group col-sm-6">
    {!! Form::label('precio', 'Precio ofertado al público:') !!}
    {!! Form::number('precio', null, ['class' => 'form-control','step' => 'any']) !!}
</div>

<!-- Preciomin Field -->
<div class="form-group col-sm-6">
    {!! Form::label('precioMin', 'Precio mínimo:') !!}
    {!! Form::number('precioMin', null, ['class' => 'form-control','step' => 'any']) !!}
</div>

<!-- Preciomax Field -->
<div class="form-group col-sm-6">
    {!! Form::label('precioMax', 'Precio máximo:') !!}
    {!! Form::number('precioMax', null, ['class' => 'form-control','step' => 'any']) !!}
</div>

<!-- Descripcion Field -->
<div class="form-group col-lg-12">
    {!! Form::label('descripcion', 'Descripción del plan:') !!}
    {!! Form::textarea('descripcion', null, ['class' => 'form-control','rows' => '4']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('plans.index') }}" class="btn btn-default">Cancelar</a>
</div>

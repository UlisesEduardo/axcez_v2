<div class="table-responsive">
    <table class="table" id="plans-table">
        <thead>
            <tr>
                <th>Id Region</th>
        <th>Nombre</th>
        <th>Subida</th>
        <th>Bajada</th>
        <th>Precio</th>
        <th>Preciomin</th>
        <th>Preciomax</th>
        <th>Descripcion</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($plans as $plan)
            <tr>
                <td>{{ $plan->id_region }}</td>
            <td>{{ $plan->nombre }}</td>
            <td>{{ $plan->subida }}</td>
            <td>{{ $plan->bajada }}</td>
            <td>{{ $plan->precio }}</td>
            <td>{{ $plan->precioMin }}</td>
            <td>{{ $plan->precioMax }}</td>
            <td>{{ $plan->descripcion }}</td>
                <td>
                    {!! Form::open(['route' => ['plans.destroy', $plan->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('plans.show', [$plan->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{{ route('plans.edit', [$plan->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>

<!-- Id Region Field -->
<div class="form-group col-sm-6">
    {!! Form::label('id_region', 'Región:') !!}
    {!! Form::select('id_region', $regionItems, null, ['class' => 'form-control']) !!}
</div>

<!-- Nombre Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nombre', 'Nombre o ubicación:') !!}
    {!! Form::text('nombre', null, ['class' => 'form-control']) !!}
</div>

<!-- Ip Field -->
<div class="form-group col-sm-6">
    {!! Form::label('ip', 'IP:') !!}
    {!! Form::text('ip', null, ['class' => 'form-control']) !!}
</div>

<!-- Gateway Field -->
<div class="form-group col-sm-6">
    {!! Form::label('gateway', 'Gateway:') !!}
    {!! Form::text('gateway', null, ['class' => 'form-control']) !!}
</div>

<!-- Notas Field -->
<div class="form-group col-lg-12">
    {!! Form::label('notas', 'Notas:') !!}
    {!! Form::textarea('notas', null, ['class' => 'form-control','rows' => '4']) !!}
</div>

<!-- Frecuencia Field -->
<div class="form-group col-sm-6">
    {!! Form::label('frecuencia', 'Frecuencia:') !!}
    {!! Form::text('frecuencia', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Gaurdar', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('sectorials.index') }}" class="btn btn-default">Cancelar</a>
</div>

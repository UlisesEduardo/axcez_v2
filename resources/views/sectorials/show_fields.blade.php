<!-- Id Region Field -->
<div class="form-group">
    {!! Form::label('id_region', 'Región:') !!}
    <p>{{ $sectorial->regionSectorial->nombreRegion }}</p>
</div>

<!-- Nombre Field -->
<div class="form-group">
    {!! Form::label('nombre', 'Nombre o ubicación:') !!}
    <p>{{ $sectorial->nombre }}</p>
</div>

<!-- Ip Field -->
<div class="form-group">
    {!! Form::label('ip', 'IP:') !!}
    <p>{{ $sectorial->ip }}</p>
</div>

<!-- Gateway Field -->
<div class="form-group">
    {!! Form::label('gateway', 'Gateway:') !!}
    <p>{{ $sectorial->gateway }}</p>
</div>

<!-- Notas Field -->
<div class="form-group">
    {!! Form::label('notas', 'Notas:') !!}
    <p>{{ $sectorial->notas }}</p>
</div>

<!-- Frecuencia Field -->
<div class="form-group">
    {!! Form::label('frecuencia', 'Frecuencia:') !!}
    <p>{{ $sectorial->frecuencia }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Registrado el:') !!}
    <p>{{ $sectorial->created_at }}</p>
</div>


<div class="table-responsive">
    <table class="table" id="contratos-table">
        <thead>
            <tr>
                <th>Nombre del cliente</th>
                <th>Folio del contrato</th>
                <th>Plan</th>
                <th>IP</th>
                <th>Gateway</th>
            </tr>
        </thead>
        <tbody>
            @foreach($contratos as $contrato)
            <tr>
                <td>{{ $contrato->cliente }}</td>
                <td>{{ $contrato->folio }}</td>
                <td>{{ $contrato->nombre }}</td>
                <td>{{ $contrato->ip }}</td>
                <td>{{ $contrato->gateway }}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
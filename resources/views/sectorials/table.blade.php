<div class="table-responsive">
    <table class="table" id="sectorials-table">
        <thead>
            <tr>
                <th>Región</th>
                <th>Nombre o ubicación</th>
                <th>IP</th>
                <th>Gateway</th>
                <th>Notas</th>
                <th>Frecuencia</th>
            </tr>
        </thead>
        <tbody>
            @foreach($sectorials as $sectorial)
            <tr>
                <td>{{ $sectorial->regionSectorial->nombreRegion }}</td>
                <td>{{ $sectorial->nombre }}</td>
                <td>{{ $sectorial->ip }}</td>
                <td>{{ $sectorial->gateway }}</td>
                <td>{{ $sectorial->notas }}</td>
                <td>{{ $sectorial->frecuencia }}</td>
                <td>
                    {!! Form::open(['route' => ['sectorials.destroy', $sectorial->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        @can('sectorials.show')
                        <a href="{{ route('sectorials.show', [$sectorial->id]) }}" class='btn btn-default'><i class="icofont-eye"></i></a>
                        @endcan
                        @can('sectorials.edit')
                        <a href="{{ route('sectorials.edit', [$sectorial->id]) }}" class='btn btn-default'><i class="icofont-ui-edit"></i></a>
                        @endcan
                        @can('sectorials.destroy')
                        {!! Form::button('<i class="icofont-ui-delete"></i>', ['type' => 'submit', 'class' => 'btn btn-danger', 'onclick' => "return confirm('Desea eliminar el registro?')"]) !!}
                        @endcan
                        {!! Form::close() !!}
                    </div>

                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    {{$sectorials -> links()}}
</div>
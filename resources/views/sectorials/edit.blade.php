@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Sectorial
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($sectorial, ['route' => ['sectorials.update', $sectorial->id], 'method' => 'patch']) !!}

                        @include('sectorials.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection
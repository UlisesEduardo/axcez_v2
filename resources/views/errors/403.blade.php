@extends('layouts.app')

@section('content')
<section class="content">
    <div class="error-page">
        <h2 class="headline text-yellow"> 403</h2>

        <div class="error-content">
            <h3><i class="fa fa-warning text-yellow"></i> Oops! Acceso no autorizado.</h3>

            <p>
                Actualmente no tiene acceso, póngase en contacto con el administrador... <a href="/home">regresar</a>
            </p>

        </div>
        <!-- /.error-content -->
    </div>
    <!-- /.error-page -->
</section>
@endsection
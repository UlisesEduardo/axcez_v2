@extends('layouts.app')

@section('content')
<section class="content">

    <div class="error-page">
        <h2 class="headline text-red">404</h2>

        <div class="error-content">
            <h3><i class="fa fa-warning text-red"></i> Oops! Objeto no localizado.</h3>

            <p>
                No hemos encontrado lo que buscas...<a href="/home">regresar</a>


        </div>
    </div>
    <!-- /.error-page -->

</section>
@endsection
<!-- Codigocliente Field -->
<div class="form-group col-sm-6">
    {!! Form::label('codigoCliente', 'Código del cliente:') !!}
    {!! Form::text('codigoCliente', null, ['class' => 'form-control','style' => 'text-transform: uppercase;']) !!}
</div>

<!-- Motivo Field -->
<div class="form-group col-lg-12">
    {!! Form::label('motivo', 'Motivo:') !!}
    {!! Form::textarea('motivo', null, ['class' => 'form-control','rows' => '4']) !!}
</div>

<!-- Fechaprogramada Field -->
<div class="form-group col-sm-6">
    {!! Form::label('fechaProgramada', 'Fecha programada de la visita:') !!}

    @if(empty($pendiente))
    <!-- verificamos si nos pasan algo, si esta vacío el control no carga nada -->
    {!! Form::date('fechaProgramada', null, ['class' => 'form-control','id'=>'fechaProgramada']) !!}
    @else
    <!-- De lo contrario nos cargan los datos -->
    {!! Form::date('fechaProgramada', $pendiente->fechaProgramada, ['class' => 'form-control','id'=>'fechaProgramada']) !!}
    @endif
    
</div>

<!-- Velocidadred Field -->
<div class="form-group col-sm-6">
    {!! Form::label('velocidadRed', 'Velocidad de red:') !!}
    {!! Form::text('velocidadRed', null, ['class' => 'form-control','placeholder' => 'subida/bajada/CCQ']) !!}
</div>
<!-- Estatus Field -->
<div class="form-group col-sm-6">
    {!! Form::label('estatus', 'Estatus:') !!}
    {!! Form::select('estatus', ['pendiente' => 'pendiente', 'resuelto' => 'resuelto'], null, ['class' => 'form-control']) !!}
</div>

<!-- Urgencia Field -->
<div class="form-group col-sm-6">
    {!! Form::label('urgencia', 'Nivel de urgencia:') !!}
    {!! Form::select('urgencia', ['bajo' => 'bajo', 'medio' => 'medio', 'alto' => 'alto'], null, ['class' => 'form-control']) !!}
</div>

<!-- Notas Field -->
<div class="form-group col-lg-12">
    {!! Form::label('notas', 'Notas adicionales:') !!}
    {!! Form::textarea('notas', null, ['class' => 'form-control','rows' => '4']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('pendientes.index') }}" class="btn btn-default">Cancelar</a>
</div>

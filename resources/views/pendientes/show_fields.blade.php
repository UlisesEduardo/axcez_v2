<!-- Codigocliente Field -->
<div class="form-group">
    {!! Form::label('codigoCliente', 'Codigo del cliente:') !!}
    <p>{{ $pendiente->codigoCliente }}</p>
</div>

<!-- Motivo Field -->
<div class="form-group">
    {!! Form::label('motivo', 'Motivo:') !!}
    <p>{{ $pendiente->motivo }}</p>
</div>

<!-- Fechaprogramada Field -->
<div class="form-group">
    {!! Form::label('fechaProgramada', 'Fecha programada:') !!}
    <p>{{ $pendiente->fechaProgramada->format('Y-m-d') }}</p>
</div>

<!-- Estatus Field -->
<div class="form-group">
    {!! Form::label('estatus', 'Estatus:') !!}
    <p>{{ $pendiente->estatus }}</p>
</div>

<!-- Urgencia Field -->
<div class="form-group">
    {!! Form::label('urgencia', 'Urgencia:') !!}
    <p>{{ $pendiente->urgencia }}</p>
</div>

<!-- Velocidadred Field -->
<div class="form-group">
    {!! Form::label('velocidadRed', 'Velocidad de red:') !!}
    <p>{{ $pendiente->velocidadRed }}</p>
</div>

<!-- Notas Field -->
<div class="form-group">
    {!! Form::label('notas', 'Notas:') !!}
    <p>{{ $pendiente->notas }}</p>
</div>

<!-- Usuario Field -->
<div class="form-group">
    {!! Form::label('usuario', 'Usuario que hizo el registro:') !!}
    <p>{{ $pendiente->usuario }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Registrado el:') !!}
    <p>{{ $pendiente->created_at }}</p>
</div>



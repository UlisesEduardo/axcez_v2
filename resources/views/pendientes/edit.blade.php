@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Pendiente
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($pendiente, ['route' => ['pendientes.update', $pendiente->id], 'method' => 'patch']) !!}

                        @include('pendientes.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection
@extends('layouts.app')
@section('content')
<section class="content-header">
  <h1>
    Pendiente
  </h1>
</section>
<div class="content">
  @include('adminlte-templates::common.errors')
  <div class="box box-primary">
    <div class="box-body">
      <div class="form-group col-sm-12">
        <div class="alert alert-success alert-dismissible">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
          <h4><i class="icon fa fa-info"></i> Importante!</h4>
          Dejar en 0 el campo velocidad de red, éste, sirve una vez que se haya hecho la visita al cliente.
        </div>
      </div>
      <div class="input-group input-group-sm col-sm-3 pull-right">
        <input type="text" id="nombresCliente" class="form-control" placeholder="Introduzca el nombre del cliente">
        <span class="input-group-btn">
          <button id="ver-clientes" href="#" data-toggle="modal" type="button" class="btn btn-info btn-flat"><i class="icofont-search-2"></i></button>
        </span>
      </div>
      </br>
      <div class="row">
        {!! Form::open(['route' => 'pendientes.store']) !!}

        @include('pendientes.fields')

        {!! Form::close() !!}
      </div>
    </div>
  </div>
</div>
@endsection


<!-- Modal -->
<div class="modal fade" id="modalClientes" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-scrollable" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="table-responsive">
          <table class="table">
            <thead>
              <th>Código</th>
              <th>Nombres</th>
              <th>Apellido paterno</th>
              <th>Apellido materno</th>
            </thead>
            <tbody id="tablaClientes">
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="table-responsive">
    <table class="table" id="pendientes-table">
        <thead>
            <tr>
                <th>Código del cliente</th>
        <th>Motivo</th>
        <th>Fecha programada</th>
        <th>Estatus</th>
        <th>Urgencia</th>
        <th>Velocidad de la red</th>
        <th>Notas</th>
        <th>Usuario</th>
            </tr>
        </thead>
        <tbody>
        @foreach($pendientes as $pendiente)
            <tr>
                <td>{{ $pendiente->codigoCliente }}</td>
            <td>{{ $pendiente->motivo }}</td>
            <td>{{ $pendiente->fechaProgramada->format('Y-m-d') }}</td>
            <td>{{ $pendiente->estatus }}</td>
            <td>{{ $pendiente->urgencia }}</td>
            <td>{{ $pendiente->velocidadRed }}</td>
            <td>{{ $pendiente->notas }}</td>
            <td>{{ $pendiente->usuario }}</td>
                <td>
                    {!! Form::open(['route' => ['pendientes.destroy', $pendiente->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        @can('pendientes.show')
                        <a href="{{ route('pendientes.show', [$pendiente->id]) }}" class='btn btn-default'><i class="icofont-eye"></i></a>
                        @endcan
                        @can('pendientes.edit')
                        <a href="{{ route('pendientes.edit', [$pendiente->id]) }}" class='btn btn-default'><i class="icofont-ui-edit"></i></a>
                        @endcan
                        @can('pendientes.destroy')
                        {!! Form::button('<i class="icofont-ui-delete"></i>', ['type' => 'submit', 'class' => 'btn btn-danger', 'onclick' => "return confirm('Desea eliminar el registro?')"]) !!}
                        @endcan
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    {{$pendientes -> links()}}
</div>

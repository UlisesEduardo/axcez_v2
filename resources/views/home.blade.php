@extends('layouts.app')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Resumen general diario <small> {{ now()->format('Y-m-d')}}</small>
    </h1>
</section>

<!-- Main content -->
<section class="content">
    <div class="col-md-12" id="msg">
        @include('flash::message')
    </div>

    <!-- Small boxes (Stat box) -->
    <div class="row">
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-aqua">
                <div class="inner">
                    <h3><sup style="font-size: 20px">-$</sup>{{$egresosCompras[0]->egresos}}/{{$egresosGastos[0]->gasto}}</h3>
                    <p>Compras/Otros gastos</p>
                </div>
                <div class="icon">
                    <i class="ion ion-bag"></i>
                </div>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-green">
                <div class="inner">
                    <h3><sup style="font-size: 20px">+$</sup>{{$ingresosContratos[0]->ingresos}}/{{$ingresosPagos[0]->pagos}}</h3>
                    <p>Contratos/Pagos</p>
                </div>
                <div class="icon">
                    <i class="ion ion-stats-bars"></i>
                </div>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-yellow">
                <div class="inner">
                    <h3>{{$clientes[0]->clientes}}</h3>
                    <p>Clientes registrados</p>
                </div>
                <div class="icon">
                    <i class="ion ion-person-add"></i>
                </div>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-red">
                <div class="inner">
                    <h3><sup style="font-size: 20px">-$</sup>{{$condonados[0]->condonados}}</h3>
                    <p>Pagos condonados</p>
                </div>
                <div class="icon">
                    <i class="glyphicon glyphicon-thumbs-down"></i>
                </div>
            </div>
        </div>
        <!-- ./col -->
    </div>
    <!-- /.row -->
    <!-- Main row -->
    <div class="row">
        <!-- Left col -->
        <section class="col-lg-7 connectedSortable">
            <!-- Caja pendientes -->
            <div class="box box-primary">
                <div class="box-header">
                    <i class="ion ion-clipboard"></i>
                    <h3 class="box-title">Pendientes programados</h3>
                </div>
                <div class="box-body">
                    <div class="table-responsive">
                        <table class="table" id="pendientes-table">
                            <thead>
                                <tr>
                                    <th>Codigo del cliente</th>
                                    <th>Motivo</th>
                                    <th>Urgencia</th>
                                    <th>Notas</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($pendientes as $pendiente)
                                <tr>
                                    <td>{{ $pendiente->codigoCliente }}</td>
                                    <td>{{ $pendiente->motivo }}</td>
                                    <td>{{ $pendiente->urgencia }}</td>
                                    <td>{{ $pendiente->notas }}</td>
                                    <td>
                                        <div class='btn-group'>
                                            <a href="{{ route('pendientes.show', [$pendiente->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                </div>

            </div>


            <!-- Caja pagos -->
            <div class="box box-primary">
                <div class="box-header">
                    <i class="glyphicon glyphicon-usd"></i>
                    <h3 class="box-title">Clientes que pagan hoy</h3>
                </div>
                <div class="box-body">
                    <div class="table-responsive">
                        <table class="table" id="regions-table">
                            <thead>
                                <tr>
                                    <th>Cliente</th>
                                    <th>Folio del contrato</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($pagos as $pago)
                                <tr>
                                    <td>{{ $pago->nombre }}</td>
                                    <td>{{ $pago->folio }}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>

            <!-- Caja productos -->
            <div class="box box-primary">
                <div class="box-header">
                    <i class="glyphicon glyphicon-tag"></i>
                    <h3 class="box-title">Productos registrados</h3>
                </div>
                <div class="box-body">
                    <div class="table-responsive">
                        <table class="table" id="productos-table">
                            <thead>
                                <tr>
                                    <th>Codigo</th>
                                    <th>Descripcion</th>
                                    <th>Cantidad</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($productos as $producto)
                                <tr>
                                    <td>{{ $producto->codigo }}</td>
                                    <td>{{ $producto->descipcion }}</td>
                                    <td>{{ $producto->cantidad }}</td>
                                    <td>
                                        <div class='btn-group'>
                                            <a href="{{ route('productos.show', [$producto->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
            <!-- Sectoriales box -->
            <div class="box box-solid bg-light-blue-gradient">
                <div class="box-header">
                    <i class="fa fa-map-marker"></i>
                    <h3 class="box-title">
                        Sectoriales
                    </h3>
                </div>
                <div class="box-body">
                    <div class="table-responsive">
                        <table class="table" id="sectorials-table">
                            <thead>
                                <tr>
                                    <th>Region</th>
                                    <th>Nombre</th>
                                    <th>Ip</th>
                                    <th>Gateway</th>
                                    <th colspan="3">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($sectoriales as $sectorial)
                                <tr>
                                    <td>{{ $sectorial->nombreRegion }}</td>
                                    <td>{{ $sectorial->nombre }}</td>
                                    <td>{{ $sectorial->ip }}</td>
                                    <td>{{ $sectorial->gateway }}</td>
                                    <td>
                                        <div class='btn-group'>
                                            <a href="{{ route('sectorials.show', [$sectorial->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>


                    <div id="world-map" style="height: 250px; width: 100%;"></div>
                </div>
                <!-- /.box-body-->
            </div>
            <!-- /.box -->

            <!-- Paquetes box -->
            <div class="box box-solid bg-light-blue-gradient">
                <div class="box-header">
                    <i class="fa fa-th"></i>
                    <h3 class="box-title">Paquetes</h3>
                </div>
                <div class="box-body border-radius-none">

                    <div class="table-responsive">
                        <table class="table" id="plans-table">
                            <thead>
                                <tr>
                                    <th>Nombre</th>
                                    <th>Subida</th>
                                    <th>Bajada</th>
                                    <th>Precio</th>
                                    <th colspan="3">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($planes as $plan)
                                <tr>
                                    <td>{{ $plan->nombre }}</td>
                                    <td>{{ $plan->subida }}</td>
                                    <td>{{ $plan->bajada }}</td>
                                    <td>{{ $plan->precio }}</td>
                                    <td>
                                        <div class='btn-group'>
                                            <a href="{{ route('plans.show', [$plan->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                    <div class="chart" id="line-chart" style="height: 250px;"></div>
                </div>
            </div>
            <!-- /.box -->

        </section>
        <!-- /.Left col -->
        <!-- right col (We are only adding the ID to make the widgets sortable)-->
        <section class="col-lg-5 connectedSortable">

            <!-- Caja pagos -->
            <div class="box box-primary">
                <div class="box-header">
                    <i class="icofont-notebook"></i>
                    <h3 class="box-title">Resumen histórico</h3>
                </div>
                <div class="box-body">

                    <!-- Info Boxes Style 2 -->
                    <div class="info-box bg-aqua">
                        <span class="info-box-icon"><i class="ion ion-bag"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">Compras y otros gastos</span>
                            <span class="info-box-number">{{$totalCompras[0]->totalCompras}}/{{$totalGastos[0]->totalGastos}}</span>
                        </div>
                    </div>
                    <!-- /.info-box-content -->

                    <!-- Info Boxes Style 2 -->
                    <div class="info-box bg-green">
                        <span class="info-box-icon"><i class="ion ion-stats-bars"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">Contratos y pagos</span>
                            <span class="info-box-number">{{$totalContratos[0]->totalContratos}}/{{$totalPagos[0]->totalPagos}}</span>
                        </div>
                    </div>
                    <!-- /.info-box-content -->

                    <!-- Info Boxes Style 2 -->
                    <div class="info-box bg-yellow">
                        <span class="info-box-icon"><i class="ion ion-person-add"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">Clientes registrados</span>
                            <span class="info-box-number">{{$clientes[0]->clientes}}</span>
                        </div>
                    </div>
                    <!-- /.info-box-content -->

                    <!-- Info Boxes Style 2 -->
                    <div class="info-box bg-red">
                        <span class="info-box-icon"><i class="glyphicon glyphicon-thumbs-down"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">Pagos condonados</span>
                            <span class="info-box-number">{{$totalCondonados[0]->totalCondonados}}</span>
                        </div>
                    </div>
                </div>

            </div>

            <!-- Caja opciones -->
            <div class="box box-primary">
                <div class="box-header">
                    <i class="icofont-options"></i>
                    <h3 class="box-title">Otras opciones</h3>
                </div>
                <div class="box-body">
                    <a class="btn btn-block btn-social btn-google" href="/helpers/generarPdf">
                        <i class="icofont-file-pdf"></i> Generar informe PDF
                    </a>
                    <a class="btn btn-block btn-social btn-linkedin" href="/helpers/respaldar">
                        <i class="icofont-database"></i> Generar respaldo de BD
                    </a>
                </div>

            </div>

        </section>
        <!-- right col -->
    </div>
    <!-- /.row (main row) -->

</section>
<!-- /.content -->
</div>

</div>
<!-- ./wrapper -->
</body>

</html>
@endsection
<!-- Importe Field -->
<div class="form-group">
    {!! Form::label('importe', 'Importe:') !!}
    <p>{{ $pago->importe }}</p>
</div>

<!-- Proximopago Field -->
<div class="form-group">
    {!! Form::label('proximoPago', 'Próximo pago:') !!}
    <p>{{ $pago->proximoPago->format('Y-m-d') }}</p>
</div>

<!-- Fechainicio Field -->
<div class="form-group">
    {!! Form::label('fechaInicio', 'Pagado del:') !!}
    <p>{{ $pago->fechaInicio->format('Y-m-d') }}</p>
</div>

<!-- Fechafin Field -->
<div class="form-group">
    {!! Form::label('fechaFin', 'Al:') !!}
    @if(empty($pago->fechaFin))
    <p>{{ $pago->fechaFin}}</p>
    @else
    <p>{{ $pago->fechaFin->format('Y-m-d') }}</p>
    @endif
</div>

<!-- Notas Field -->
<div class="form-group">
    {!! Form::label('notas', 'Notas:') !!}
    <p>{{ $pago->notas }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Pago realizado el:') !!}
    <p>{{ $pago->created_at->format('Y-m-d') }}</p>
</div>



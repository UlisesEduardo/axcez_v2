<!-- Folio contrato-->
<div class="form-group col-sm-6">
    {!! Form::label('folioContrato', 'Folio del contrato:') !!}
    {!! Form::text('folioContrato', null, ['class' => 'form-control','style' => 'text-transform: uppercase;','required' => 'required','pattern' => '{7}']) !!}
</div>

<!-- Importe Field -->
<div class="form-group col-sm-6">
    {!! Form::label('importe', 'Importe:') !!}
    {!! Form::number('importe', null, ['class' => 'form-control','required' => 'required','pattern' => '[0-9]+([\.,][0-9]+)?']) !!}
</div>

<!-- Proximopago Field -->
<div class="form-group col-sm-6">
    {!! Form::label('proximoPago', 'Próximo pago:') !!}
    {!! Form::date('proximoPago', null, ['class' => 'form-control','id'=>'proximoPago','required' => 'required']) !!}
</div>

<!-- Fechainicio Field -->
<div class="form-group col-sm-6">
    {!! Form::label('fechaInicio', 'Pago del:') !!}
    {!! Form::date('fechaInicio', null, ['class' => 'form-control','id'=>'fechaInicio','required' => 'required']) !!}
</div>

<!-- Fechafin Field -->
<div class="form-group col-sm-6">
    {!! Form::label('fechaFin', 'Al:') !!}
    {!! Form::date('fechaFin', null, ['class' => 'form-control','id'=>'fechaFin']) !!}
</div>

<!-- Condonado Field -->
<input type="checkbox" id="condonado" name="condonado" value="1">
<label for="condonado"> Pago condonado</label><br>

<!-- Notas Field -->
<div class="form-group col-lg-12">
    {!! Form::label('notas', 'Notas:') !!}
    {!! Form::textarea('notas', null, ['class' => 'form-control','rows' => '4']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    <button id="guardarPago" pagos-url="{{route('pagos.store')}}" class="btn btn-primary">Guardar</button>
    <a href="{{ route('pagos.index') }}" class="btn btn-default">Cancelar</a>
</div>

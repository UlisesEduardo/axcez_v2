@section('contentPanel')
<div class="table-responsive">
    <table class="table" id="pagos-table">
        <thead>
            <tr>
                <th>Importe</th>
                <th>Próximo pago</th>
                <th>Pagado del </th>
                <th>Al</th>
                <th>Notas</th>
            </tr>
        </thead>
        <tbody>
            @foreach($pagos as $pago)
            <tr>
                <td>{{ $pago->importe }}</td>
                <td>{{ $pago->proximoPago }}</td>
                <td>{{ $pago->fechaInicio }}</td>
                 <!-- No es necesario aplicar formato ya que lo trae en YYYY-mm-dd -->
                <td>{{ $pago->fechaFin }}</td>
                <td>{{ $pago->notas }}</td>
                <td>
                    <div class='btn-group'>
                        @can('pagos.show')
                        <a href="{{ route('pagos.show', [$pago->id]) }}" class='btn btn-default'><i class="icofont-eye"></i></a>
                        @endcan
                    </div>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection
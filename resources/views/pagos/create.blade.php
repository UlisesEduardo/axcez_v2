@extends('layouts.app')

@section('content')
<section class="content-header">
  <h1>
    Pago
  </h1>
</section>
<div class="content">
  @include('adminlte-templates::common.errors')
  <div class="box box-primary">
    <div class="box-body">
      <div class="input-group input-group-sm col-sm-3 pull-right">
        <input type="text" id="nombre_cliente" class="form-control" placeholder="Introduzca el nombre del cliente">
        <span class="input-group-btn">
          <button id="ver-contrato" href="#" data-toggle="modal" type="button" class="btn btn-info btn-flat"><i class="icofont-search-2"></i></button>
        </span>
      </div>
      </br>
      <div class="row">
        <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
        {!! Form::open(['route' => 'pagos.store']) !!}

        @include('pagos.fields')

        {!! Form::close() !!}
      </div>
    </div>
  </div>
</div>
@endsection

<!-- Modal -->
<div class="modal fade" id="modalContratos" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-scrollable" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="table-responsive">
          <table class="table">
            <thead>
              <th>Nombre completo</th>
              <th>Barrio</th>
              <th>Calle</th>
              <th>No</th>
              <th>Teléfono</th>
              <th>Folio de contrato</th>
            </thead>
            <tbody id="tablaContratos">
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
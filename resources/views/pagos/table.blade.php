<div class="table-responsive">
    <table class="table" id="pagos-table">
        <thead>
            <tr>
                <th>Importe</th>
                <th>Próximo pago</th>
                <th>Pagado del </th>
                <th>Al</th>
                <th>Notas</th>
            </tr>
        </thead>
        <tbody>
            @foreach($pagos as $pago)
            <tr>
                <td>{{ $pago->importe }}</td>
                <td>{{ $pago->proximoPago->format('Y-m-d') }}</td>
                <td>{{ $pago->fechaInicio->format('Y-m-d') }}</td>
                @if(empty($pago->fechaFin))
                <!-- verificamos si nos pasan algo, si esta vacío el control no carga nada -->
                <td>{{ $pago->fechaFin }}</td>
                @else
                <!-- De lo contrario nos cargan los datos -->
                <td>{{ $pago->fechaFin->format('Y-m-d') }}</td>
                @endif
                <td>{{ $pago->notas }}</td>
                <td>
                    <div class='btn-group'>
                        @can('pagos.show')
                        <a href="{{ route('pagos.show', [$pago->id]) }}" class='btn btn-default'><i class="icofont-eye"></i></a>
                        @endcan
                    </div>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    {{$pagos -> links()}}
</div>
<div class="table-responsive">
    <table class="table" id="proveedors-table">
        <thead>
            <tr>
                <th>Nombre</th>
        <th>Teléfono contacto</th>
        <th>Teléfonosecundario</th>
        <th>Correo electrónico</th>
        <th>Encargado</th>
        <th>Ubicación</th>
        <th>Página web</th>
            </tr>
        </thead>
        <tbody>
        @foreach($proveedors as $proveedor)
            <tr>
                <td>{{ $proveedor->nombre }}</td>
            <td>{{ $proveedor->telefonoContacto }}</td>
            <td>{{ $proveedor->telefonoSecundario }}</td>
            <td>{{ $proveedor->correo }}</td>
            <td>{{ $proveedor->encargado }}</td>
            <td>{{ $proveedor->ubicacion }}</td>
            <td>{{ $proveedor->pagina }}</td>
                <td>
                    {!! Form::open(['route' => ['proveedors.destroy', $proveedor->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        @can('proveedors.show')
                        <a href="{{ route('proveedors.show', [$proveedor->id]) }}" class='btn btn-default'><i class="icofont-eye"></i></a>
                        @endcan
                        @can('proveedors.edit')
                        <a href="{{ route('proveedors.edit', [$proveedor->id]) }}" class='btn btn-default'><i class="icofont-ui-edit"></i></a>
                        @endcan
                        @can('proveedors.destroy')
                        {!! Form::button('<i class="icofont-ui-delete"></i>', ['type' => 'submit', 'class' => 'btn btn-danger', 'onclick' => "return confirm('Desea eliminar el registro?')"]) !!}
                        @endcan
                        {!! Form::button('<i class="icofont-brand-whatsapp"></i>', [ 'class' => 'btn btn-outline-success', 'onclick' => "  window.open('https://api.whatsapp.com/send?phone=52'+$proveedor->telefonoContacto);"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    {{$proveedors -> links()}}
</div>

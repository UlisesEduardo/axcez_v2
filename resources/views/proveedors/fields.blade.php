<!-- Nombre Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nombre', 'Nombre:') !!}
    {!! Form::text('nombre', null, ['class' => 'form-control']) !!}
</div>

<!-- Telefonocontacto Field -->
<div class="form-group col-sm-6">
    {!! Form::label('telefonoContacto', 'Teléfono contacto:') !!}
    {!! Form::text('telefonoContacto', null, ['class' => 'form-control']) !!}
</div>

<!-- Telefonosecundario Field -->
<div class="form-group col-sm-6">
    {!! Form::label('telefonoSecundario', 'Teléfono secundario:') !!}
    {!! Form::text('telefonoSecundario', null, ['class' => 'form-control']) !!}
</div>

<!-- Correo Field -->
<div class="form-group col-sm-6">
    {!! Form::label('correo', 'Correo electrónico:') !!}
    {!! Form::text('correo', null, ['class' => 'form-control']) !!}
</div>

<!-- Encargado Field -->
<div class="form-group col-sm-6">
    {!! Form::label('encargado', 'Encargado:') !!}
    {!! Form::text('encargado', null, ['class' => 'form-control']) !!}
</div>

<!-- Ubicacion Field -->
<div class="form-group col-sm-6">
    {!! Form::label('ubicacion', 'Ubicación:') !!}
    {!! Form::text('ubicacion', null, ['class' => 'form-control']) !!}
</div>

<!-- Pagina Field -->
<div class="form-group col-sm-6">
    {!! Form::label('pagina', 'Página web:') !!}
    {!! Form::text('pagina', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('proveedors.index') }}" class="btn btn-default">Cancelar</a>
</div>

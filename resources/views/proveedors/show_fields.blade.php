<!-- Nombre Field -->
<div class="form-group">
    {!! Form::label('nombre', 'Nombre:') !!}
    <p>{{ $proveedor->nombre }}</p>
</div>

<!-- Telefonocontacto Field -->
<div class="form-group">
    {!! Form::label('telefonoContacto', 'Teléfono contacto:') !!}
    <p>{{ $proveedor->telefonoContacto }}</p>
</div>

<!-- Telefonosecundario Field -->
<div class="form-group">
    {!! Form::label('telefonoSecundario', 'Teléfono secundario:') !!}
    <p>{{ $proveedor->telefonoSecundario }}</p>
</div>

<!-- Correo Field -->
<div class="form-group">
    {!! Form::label('correo', 'Correo electrónico:') !!}
    <p>{{ $proveedor->correo }}</p>
</div>

<!-- Encargado Field -->
<div class="form-group">
    {!! Form::label('encargado', 'Encargado:') !!}
    <p>{{ $proveedor->encargado }}</p>
</div>

<!-- Ubicacion Field -->
<div class="form-group">
    {!! Form::label('ubicacion', 'Ubicación:') !!}
    <p>{{ $proveedor->ubicacion }}</p>
</div>

<!-- Pagina Field -->
<div class="form-group">
    {!! Form::label('pagina', 'Página web:') !!}
    <p>{{ $proveedor->pagina }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Registrado el:') !!}
    <p>{{ $proveedor->created_at }}</p>
</div>


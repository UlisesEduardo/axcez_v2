@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1 class="pull-left">Clientes</h1>
    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
            <div class="input-group input-group-sm col-sm-3">
                <input type="text" id="datosCliente" class="form-control" placeholder="Buscar cliente">
                <span class="input-group-btn">
                    <button id="buscarDatos" url="/helpers/datosCliente" href="#" data-toggle="modal" type="button" class="btn btn-info btn-flat"><i class="icofont-search-2"></i></button>
                </span>
            </div>
            </br>
            <div id="principalPanel">
                @section('contentPanel')
                <!-- Solo se recarga esta parte, por eso va dentro este include, sino no desaparece-->
                @include('clientes.table')
                @show
            </div>
            </div>
        </div>
        <div class="text-center">
        
        </div>
    </div>
@endsection


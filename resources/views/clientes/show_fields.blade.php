<div class="box-header with-border">
    <i class="icofont-user-alt-1"></i>

    <h3 class="box-title">Datos generales</h3>
</div>
<div class="box-body">
    <!-- Codigo Field -->
    <div class="form-group">
        {!! Form::label('codigo', 'Código del cliente:') !!}
        <p>{{ $cliente->codigo }}</p>
    </div>

    <!-- Nombres Field -->
    <div class="form-group">
        {!! Form::label('nombres', 'Nombre(s):') !!}
        <p>{{ $cliente->nombres }}</p>
    </div>

    <!-- Apellidopaterno Field -->
    <div class="form-group">
        {!! Form::label('apellidoPaterno', 'Apellido paterno:') !!}
        <p>{{ $cliente->apellidoPaterno }}</p>
    </div>

    <!-- Apellidomaterno Field -->
    <div class="form-group">
        {!! Form::label('apellidoMaterno', 'Apellido materno:') !!}
        <p>{{ $cliente->apellidoMaterno }}</p>
    </div>
</div>


<div class="box-header with-border">
    <i class="icofont-location-pin"></i>

    <h3 class="box-title">Datos del domicilio</h3>
</div>
<div class="box-body">
    <!-- Municipio Field -->
    <div class="form-group">
        {!! Form::label('municipio', 'Municipio:') !!}
        <p>{{ $cliente->municipio }}</p>
    </div>

    <!-- Barrio Field -->
    <div class="form-group">
        {!! Form::label('barrio', 'Barrio:') !!}
        <p>{{ $cliente->barrio }}</p>
    </div>

    <!-- Calle Field -->
    <div class="form-group">
        {!! Form::label('calle', 'Calle:') !!}
        <p>{{ $cliente->calle }}</p>
    </div>

    <!-- Num Field -->
    <div class="form-group">
        {!! Form::label('num', 'No:') !!}
        <p>{{ $cliente->num }}</p>
    </div>

    <!-- Referencia Field -->
    <div class="form-group">
        {!! Form::label('referencia', 'Referencia del domicilio:') !!}
        <p>{{ $cliente->referencia }}</p>
    </div>

    <!-- Coordenadas Field -->
    <div class="form-group">
        {!! Form::label('coordenadas', 'Coordenadas:') !!}
        <p>{{ $cliente->coordenadas }}</p>
    </div>
</div>


<div class="box-header with-border">
    <i class="icofont-ui-touch-phone"></i>

    <h3 class="box-title">Datos de contacto</h3>
</div>

<div class="box-body">
    <!-- Telefono Field -->
    <div class="form-group">
        {!! Form::label('telefono', 'Teléfono:') !!}
        <p>{{ $cliente->telefono }}</p>
    </div>

    <!-- Telefonoalternativo Field -->
    <div class="form-group">
        {!! Form::label('telefonoAlternativo', 'Teléfono alternativo:') !!}
        <p>{{ $cliente->telefonoAlternativo }}</p>
    </div>

    <!-- Correo Field -->
    <div class="form-group">
        {!! Form::label('correo', 'Correo electrónico:') !!}
        <p>{{ $cliente->correo }}</p>
    </div>

    <!-- Created At Field -->
    <div class="form-group">
        {!! Form::label('created_at', 'Registrado el:') !!}
        <p>{{ $cliente->created_at }}</p>
    </div>
</div>
@section('contentPanel')
<div class="table-responsive">
    <table class="table" id="clientes-table">
        <thead>
            <tr>
                <th>Código</th>
                <th>Nombre(s)</th>
                <th>Apellido paterno</th>
                <th>Apellido materno</th>
                <th>Municipio</th>
                <th>Barrio</th>
                <th>Calle</th>
                <th>No</th>
                <th>Referencia</th>
                <th>Teléfono</th>
            </tr>
        </thead>
        <tbody>
            @foreach($clientes as $cliente)
            <tr>
                <td>{{ $cliente->codigo }}</td>
                <td>{{ $cliente->nombres }}</td>
                <td>{{ $cliente->apellidoPaterno }}</td>
                <td>{{ $cliente->apellidoMaterno }}</td>
                <td>{{ $cliente->municipio }}</td>
                <td>{{ $cliente->barrio }}</td>
                <td>{{ $cliente->calle }}</td>
                <td>{{ $cliente->num }}</td>
                <td>{{ $cliente->referencia }}</td>
                <td>{{ $cliente->telefono }}</td>
                <td>
                    {!! Form::open(['route' => ['clientes.destroy', $cliente->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        @can('clientes.show')
                        <a href="{{ route('clientes.show', [$cliente->id]) }}" class='btn btn-default'><i class="icofont-eye"></i></a>
                        @endcan
                        @can('clientes.edit')
                        <a href="{{ route('clientes.edit', [$cliente->id]) }}" class='btn btn-default'><i class="icofont-ui-edit"></i></a>
                        @endcan
                        @can('clientes.destroy')
                        {!! Form::button('<i class="icofont-ui-delete"></i>', ['type' => 'submit', 'class' => 'btn btn-danger', 'onclick' => "return confirm('Desea eliminar el registro?')"]) !!}
                        @endcan
                        {!! Form::button('<i class="icofont-brand-whatsapp"></i>', [ 'class' => 'btn btn-outline-success', 'onclick' => " window.open('https://api.whatsapp.com/send?phone=52'+$cliente->telefono);"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>

@endsection
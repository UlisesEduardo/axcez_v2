<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::get('/home', 'HomeController@index');

Route::middleware(['auth'])->group(function () {

    Route::resource('clientes', 'ClienteController');

    Route::resource('regions', 'RegionController');

    Route::resource('plans', 'PlanController');

    Route::resource('sectorials', 'SectorialController');

    Route::resource('prospectos', 'ProspectoController');

    Route::resource('productos', 'ProductoController');

    Route::resource('gastos', 'GastosController');

    Route::resource('pendientes', 'PendienteController');

    Route::resource('compras', 'ComprasController');

    Route::resource('contratos', 'ContratoController');

    Route::resource('pagos', 'PagoController');

    Route::resource('proveedors', 'ProveedorController');

    Route::resource('users', 'UserController');

    Route::resource('roles', 'RoleController');
    //Rutas auxiliares
    Route::get('/helpers/buscar', 'HelperController@busqueda');

    Route::get('/helpers/buscarProducto', 'HelperController@buscaProducto');

    Route::get('/helpers/verificarExistencias', 'HelperController@verificarStock');

    Route::get('/helpers/buscarContrato', 'HelperController@verificarContrato');

    Route::get('/helpers/buscarFolio', 'HelperController@verificarFolio');

    Route::get('/helpers/buscarSector', 'HelperController@verificarSector');

    Route::get('/helpers/contratoCliente', 'HelperController@contratoCliente');

    Route::get('/helpers/pagoCliente', 'HelperController@pagoCliente');

    Route::get('/helpers/datosCliente', 'HelperController@datosCliente');

    Route::get('/helpers/generarPdf', 'HelperController@generarPdf');

    Route::get('/helpers/respaldar', 'HelperController@respaldar');
});


